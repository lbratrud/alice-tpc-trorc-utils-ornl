## T-RORC Utilities

See [NOTES](NOTES.md)

### Tools
Common tool options are:
  * `-h | --help`<br/>Displays the tool help
  * `--version`<br/>Display version information extracted from the system and git
  * `-m | --mask`<br/>Specify bit mask of FECs/SCAs to apply the commands to

### Setup
The software expects the following wiring scheme:
  * `FEC0, GBTx0 --> DMA CH0`
  * `FEC0, GBTx1 --> DMA CH1`
  * `FEC1, GBTx0 --> DMA CH2`
  * `FEC1, GBTx1 --> DMA CH3`
  * `...`

The QSFP connector for the first four channels is the one closest to the LED.

### Requirements
A somewhat modern compiler, install e.g. gcc5 via software collections<br/>
  `sudo yum install centos-release-scl-rh`<br/>
  `sudo yum install devtoolset-4-gcc-c++`<br/>
  `. /opt/rh/devtoolset-4/enable`<br/>
gcc6 doesn't work with boost-1.53.0 because of bug in boost::multiprecision
