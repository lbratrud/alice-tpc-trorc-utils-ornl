#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <vector>
#include <boost/program_options.hpp>

#include "trorc.hpp"
#include "gbt_link.hpp"
#include "gbt_sca_comm.hpp"
#include "fec.hpp"
#include "git_info.hpp"

using namespace gbt;
namespace bpo = boost::program_options;

void fecInit(Fec& fec,int CG0,int CG1,int CTS,int POL,int init,int V2)
{
  fec.init(CG0,CG1,CTS,POL,init,V2);

  if (init==1)
  {
     ScaGpio& gpio(fec.getScaGpio());
     std::cout << gpio << std::endl;
     ScaAdc& adc(fec.getScaAdc());
     std::cout << adc << std::endl;
 }
}

void gbtxI2cScan(Fec& fec)
{
  fec.printIdString(std::cout);
  std::cout << "Scanning GBTx I2C ports for active devices\n";
  fec.gbtxI2cScan(0, &std::cout);
  fec.gbtxI2cScan(1, &std::cout);
}

void gbtxSetI2cAddr(Fec& fec, uint8_t gbtx_idx, uint32_t val)
{
  ScaI2c& i2c(fec.getScaI2cGbtx(gbtx_idx));
  fec.printIdString(std::cout);
  std::cout << "  Set GBTx" << static_cast<uint32_t>(gbtx_idx)
            << " I2C slave address temporarily to "
            << i2c.setSlaveAddress(val) << "\n";
}


int main(int argc, char** argv)
{
  bpo::variables_map vm;
  bpo::options_description opt_general(
    "Usage:\n  " + std::string(argv[0]) + " <cmds/options>\n"
    "  Tool will apply the operations on all FECs/SCAs defined by the corresponding mask\n"
    "Commands / Options");
  bpo::options_description opt_hidden("");
  bpo::options_description opt_all;
  bpo::positional_options_description opt_pos;

  try
  {
    opt_general.add_options()
      // Careful with changing option positions here, might have effect
      //   on program
      ("help,h", "Print this help message")
      ("verbose,v", "Be verbose")
      ("version", "Print version information")
      ("mask,m", bpo::value<cl_uint_t>()->default_value(0x1),
         "Mask defining on which FECs/SCAs to execute all following operations")
      //
      ("init", "Initialize FEC with basic configuration")

      // gbtx*-addr needs to come before other gbtx* options
      ("gbtx0-addr", bpo::value<cl_uint_t>(),
         "Use alternative GBTx0 I2C address for following GBTx0 operations [1,..,15]")
      ("gbtx1-addr", bpo::value<cl_uint_t>(),
         "Use alternative GBTx1 I2C address for following GBTx1 operations [1,..,15]")
      ("gbtx-scan", "Scan for GBTx (addresses) on I2C")
      ("gbtx0-cfg", bpo::value<std::string>(), "Configure GBTx0 with file")
      ("gbtx1-cfg", bpo::value<std::string>(), "Configure GBTx1 with file")
      ("gbtx0-cfg-dump", "Dump GBTx0 configuration")
      ("gbtx1-cfg-dump", "Dump GBTx1 configuration")
      //
      ("status,s", "Print FEC status")
      ("cg0", bpo::value<cl_uint_t>()->default_value(0x0),"set CG0 gain (20mV/fC). 1=>30mV/fC")
      ("cg1", bpo::value<cl_uint_t>()->default_value(0x1),"set CG1 gain (20mV/fC). 0=3mV/fC, 1=20mV/fC")
      ("cts", bpo::value<cl_uint_t>()->default_value(0x0),"set CTS shaping (160ms)")
      ("pol", bpo::value<cl_uint_t>()->default_value(0x1),"set Polarity (Default=1=Neg 0=Pos)")
      ("v2","setup for V2 SAMPA")
      ("adctest,a", bpo::value<cl_uint_t>(),"Do ADC tests")
      ;
    opt_all.add(opt_general).add(opt_hidden);

    bpo::store(bpo::command_line_parser(argc, argv).options(opt_all)
                     .positional(opt_pos).run(), vm);

    if (vm.count("help") || argc == 1) {
      std::cout << opt_general << std::endl;
      exit(0);
    }

    bpo::notify(vm);
  }
  catch(bpo::error& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << opt_general << std::endl;
    exit(1);
  }
  catch(std::exception& e)
  {
    std::cerr << e.what() << ", application will now exit" << std::endl;
    exit(2);
  }


  // Do stuff
  int device_number(0);
  int bar_number(1);
  std::unique_ptr<trorc::Device> trorc;
  std::unique_ptr<trorc::Bar> bar;

  try {
    trorc.reset(new trorc::Device(device_number));
    bar.reset(new trorc::Bar(*trorc, bar_number));
  } catch (int e) {
    std::cerr << "ERROR: Failed to initialize T-RORC: " << librorc::errMsg(e)
              << std::endl;
    exit(1);
  }

  if (vm.count("version"))
    std::cout << GitInfo();
  int CG0=0,CG1=1,CTS=0;

  try {
    // FEC, or SCA loop
    for (uint32_t fec_idx = 0; fec_idx < 32; fec_idx++) {
      // Specified FECs/SCAs
      if ((vm["mask"].as<cl_uint_t>()() >> fec_idx) & 0x1) {

        GscTrorcHdlcSw sca_comm(*bar, fec_idx, true, false);

        if (vm.count("verbose"))
          std::cout << "Communcating with SCA" << fec_idx << std::endl;

        Fec fec(sca_comm);

        if (vm.count("verbose"))
         fec.setVerbosity(10);

        if (vm.count("verbose"))
          fecInit(fec, vm["cg0"].as<cl_uint_t>()(), vm["cg1"].as<cl_uint_t>()(), vm["cts"].as<cl_uint_t>()(), vm["pol"].as<cl_uint_t>()(),vm.count("init"),vm.count("v2"));

        if (vm.count("gbtx-scan"))
          gbtxI2cScan(fec);

        if (vm.count("gbtx0-addr"))
          gbtxSetI2cAddr(fec, 0, static_cast<uint32_t>(vm["gbtx0-addr"].as<cl_uint_t>()()));
        if (vm.count("gbtx1-addr"))
          gbtxSetI2cAddr(fec, 1, static_cast<uint32_t>(vm["gbtx1-addr"].as<cl_uint_t>()()));

        if (vm.count("gbtx0-cfg"))
          fec.gbtxConfigure(0, vm["gbtx0-cfg"].as<std::string>());
        if (vm.count("gbtx1-cfg"))
          fec.gbtxConfigure(1, vm["gbtx1-cfg"].as<std::string>());

        if (vm.count("gbtx0-cfg-dump"))
          fec.gbtxDumpConfiguration(0, std::cout);
        if (vm.count("gbtx1-cfg-dump"))
          fec.gbtxDumpConfiguration(1);

	if (vm.count("adctest"))
	{
	   fec.getAdcSamples(std::cout,fec,static_cast<uint32_t>(vm["adctest"].as<cl_uint_t>()()));
	}
        if (vm.count("status"))
          std::cout << fec << "\n";
      } // Specified FECs/SCAs
    } // FEC/SCA loop
  }
  catch (std::exception& e) {
    std::cerr << e.what() << ", exiting" << std::endl;
    exit(100);
  }

  return 0;
}
