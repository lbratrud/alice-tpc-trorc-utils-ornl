#pragma once

#include <memory>
#include <stdexcept>
#include <librorc.h>

#include "utils.hpp"

#ifdef TRORC_DEBUG
  #define tdebug(s) std::cout << s << std::endl;
#else
  #define tdebug(...)
#endif

//------------------------------------------------------------------------------
// ...as long as there's no make_unique in C++
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique(Args&& ...args)
{
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}


//------------------------------------------------------------------------------
namespace trorc {

/** T-RORC wrapper class for librorc::device
 */
class Device
{
  private:
    std::unique_ptr<librorc::device> dev_;

  public:
    /** Create T-RORC device
     *  @param device_number Device number
     */
    Device(int device_number = 0) :
        dev_(new librorc::device(device_number))
      {}

    /** Get underlying librorc device pointer
     */
    inline librorc::device* const get() const { return dev_.get(); }
};



/** T-RORC wrapper class for librorc::bar with
 *  more flexible bar read/write
 */
class Bar
{
  private:
    /** Calculate bit mask (32-bit)
      * @return 32-bit mask with bits [width+lsb-1 .. lsb] set to one
      */
    inline uint32_t
    mask(uint8_t width, uint8_t lsb = 0) const
      { return ((width == 32) ? 0xffffffff : ((1 << width) - 1)) << lsb; }

    std::unique_ptr<librorc::bar> bar_;

  public:
    /** Create Bar
     *  @param dev        T-RORC device the bar gets is attached to
     *  @param bar_number Bar number
     */
    Bar(const Device& dev, int bar_number = 1) :
        bar_(new librorc::bar(dev.get(), bar_number))
      {}

    /** Get underlying librorc bar pointer
     */
    inline librorc::bar* const get() const { return bar_.get(); }

    /** Extract field from value
     *  @return Content of [lsb+width-1 .. lsb] of vin
     */
    inline uint32_t
    getField(uint32_t vin, uint8_t lsb, uint8_t width) const
      { return (vin >> lsb) & mask(width); }

    /** Extract / test bit value
     *  @return Value of bit 'idx' of 'vin'
     */
    inline uint8_t
    getBit(uint32_t vin, uint8_t idx) const
      { return getField(vin, idx, 1); }


    /** Read field from hardware
     *  @return Content of [lsb+width-1 .. lsb] at address 'addr'
     */
    inline uint32_t
    readField(uint32_t addr, uint8_t lsb, uint8_t width) const
      { return getField(read(addr), lsb, width); }

    /** Read bit from hardware
     *  @return Value of bit 'idx' at address 'addr'
     */
    inline uint8_t
    readBit(uint32_t addr, uint8_t idx) const
      { return readField(addr, idx, 1); }


    /** Write field, leaving all other bits unchanged
    *   @return Content of [lsb+width-1 .. lsb] at address 'addr' (read-back)
    */
    inline uint32_t
    writeField(uint32_t addr, uint8_t lsb, uint8_t width, uint32_t value) const
      {
        uint32_t r = read(addr) & ~mask(width, lsb);
        write(addr, r | ((value & mask(width)) << lsb));
        return readField(addr, lsb, width);
      }

    /** Write bit, leaving all other bits unchanged
     *  @return Value of bit 'idx' at address 'addr' (read-back)
     */
    inline uint32_t
    writeBit(uint32_t addr, uint8_t idx, uint32_t value) const
      { return writeField(addr, idx, 1, value); }


    /** Read 32 bit
     *  @return 32-bit value at address 'addr'
     */
    inline uint32_t
    read(uint32_t addr) const
      { return bar_->get32(addr); }

    /** Write 32 bit
     *  @return 32-bit value at address 'addr' (read-back)
     */
    inline uint32_t
    write(uint32_t addr, uint32_t data) const
      {
        bar_->set32(addr, data);
        return bar_->get32(addr);
      }
};



/** T-RORC exception class
 */
class Exception : public std::exception
{
  private:
    std::string what_arg;

  public:
    Exception(const std::string& s) : what_arg("ERROR : T-RORC : " + s) {}
    virtual const char* what() const noexcept { return what_arg.c_str(); }
};

}  // namespace trorc
