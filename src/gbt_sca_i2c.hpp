#pragma once
#include "gbt_sca.hpp"

namespace gbt {

//------------------------------------------------------------------------------
/** GBT SCA I2C communication base class
 */
class ScaI2c : public Sca
{
  protected:
    /** Valid I2C clock frequencies
     */
    enum i2c_freq_t : uint32_t {
        FREQ_100k = 0x0, 
        FREQ_200k = 0x1, 
        FREQ_400k = 0x2, 
        FREQ_1M   = 0x3
      };

    const i2c_freq_t i2c_freq_{FREQ_1M};  /**< I2C clock frequency */

    uint32_t cfg_slave_daddr_; /**< Internally stored slave device address, used in the
                                *   read/write variants with slave register addressing only
                                */

    inline bool isSuccess(uint8_t status) const  { return (status & 0x4) == 0x4; }

  public:

    /** Construct ScaI2c instance
     *  @param sca_comm Reference to the SCA communication
     *  @param i2c_if_idx I2C interface index (0..15)
     *  @param slave_i2c_addr Slave I2C address used when using the
     *         read/write variants with slave register addressing only
     */
    ScaI2c(ScaComm& sca_comm, uint8_t i2c_if_idx = 0, uint32_t slave_i2c_addr = 0) :
        Sca(sca_comm, 0x3 + i2c_if_idx),
        cfg_slave_daddr_(slave_i2c_addr)
      {}

    /** Get I2C slave device address
     *  @return Slave device address used in the r/w variants with slave register addressing
     */
    inline uint32_t getSlaveAddress() const { return cfg_slave_daddr_; }

    /** Set I2C slave device address
     *  @param addr New I2C slave device address [1, .., 15]
     *  @return New slave device address value
     */
    inline uint32_t setSlaveAddress(uint32_t addr)
      {
         if ((addr >= 1) && (addr <= 15))
           cfg_slave_daddr_ = addr;
         return getSlaveAddress();
      }

    /** Selectively set the I2C frequency
     *  @param freq Frequency
     *  @return Read-back frequency setting
     */
    inline i2c_freq_t setFreq() const
      {
        return static_cast<i2c_freq_t>(setControl((getControl() & 0xfffffffc) | i2c_freq_) & 0x3);
      }

    /** Read byte from I2C slave using the stored slave device address
     *  @param raddr Slave register address
     *  @return Byte received from slave at [cfg_slave_daddr_, raddr]
     *  @see cfg_slave_daddr_
     *  @throws ScaException
     */
    inline uint8_t readByte(uint32_t raddr) const
      {
        return readByte(cfg_slave_daddr_, raddr);
      }

    /** Read byte from I2C slave
     *  @param daddr Slave device address
     *  @param raddr Slave register address
     *  @return Received byte
     *  @throws ScaException
     */
    virtual uint8_t readByte(uint32_t daddr, uint32_t raddr) const = 0;

    /** Write byte to I2C slave using the stored slave device address
     *  @param raddr Slave register address
     *  @return Value read back from slave at [cfg_slave_daddr_, raddr]
     *  @see cfg_slave_daddr_
     *  @throws ScaException
     */
    inline uint8_t writeByte(uint32_t raddr, uint8_t value) const
      {
        return writeByte(cfg_slave_daddr_, raddr, value);
      }

    /** Write byte to I2C slave
     *  @param daddr Slave device address
     *  @param raddr Slave register address
     *  @return Value read back from slave at [daddr,raddr]
     *  @throws ScaException
     */
    virtual uint8_t writeByte(uint32_t daddr, uint32_t raddr, uint8_t value) const = 0;

    /** Get CONTROL register
     *  @return CONTROL register value
     */
    inline uint32_t getControl() const { return transceive(I2C_R_CTRL).data; }

    /** Set CONTROL register
     *  @param ctrl Register value
     *  @return Read-back CONTROL register value
     */
    inline uint32_t setControl(uint32_t ctrl) const
      {
        transceive(I2C_W_CTRL, ctrl);
        return getControl();
      }

    /** Selectively set NBYTE field of CONTROL register
     *  @note Only evaluated during multi-byte transmissions (SCA manual v8.0, p18)
     *  @param nbyte Value of the NBYTE field [0..16]
     *  @return Read-back NBYTE value
     */
    inline uint32_t setNbyte(uint8_t nbyte) const
      {
        return ((setControl((getControl() & 0xffffff83) | ((nbyte & 0x1f) << 2))) >> 2) & 0x1f;
      }

    inline uint32_t getStatus() const { return transceive(I2C_R_STAT).data; }

    const sca_rgf_t I2C_R_STAT  {0x11, 2, 2, BO_NORMAL};
    const sca_rgf_t I2C_W_MASK  {0x20, 2, 2, BO_NORMAL};
    const sca_rgf_t I2C_R_MASK  {0x21, 2, 2, BO_NORMAL};
    const sca_rgf_t I2C_W_CTRL  {0x30, 2, 2, BO_NORMAL};
    const sca_rgf_t I2C_R_CTRL  {0x31, 2, 2, BO_NORMAL};

    const sca_rgf_t I2C_W_DATA0 {0x40, 4, 4, BO_NORMAL};
    const sca_rgf_t I2C_R_DATA0 {0x41, 4, 4, BO_NORMAL};
    const sca_rgf_t I2C_W_DATA1 {0x50, 4, 4, BO_NORMAL};
    const sca_rgf_t I2C_R_DATA1 {0x51, 4, 4, BO_NORMAL};
    const sca_rgf_t I2C_W_DATA2 {0x60, 4, 4, BO_NORMAL};
    const sca_rgf_t I2C_R_DATA2 {0x61, 4, 4, BO_NORMAL};
    const sca_rgf_t I2C_W_DATA3 {0x70, 4, 4, BO_NORMAL};
    const sca_rgf_t I2C_R_DATA3 {0x71, 4, 4, BO_NORMAL};

    const sca_rgf_t I2C_S_7B_W  {0x82, 2, 2, BO_NORMAL};
    const sca_rgf_t I2C_S_7B_R  {0x86, 2, 2, BO_NORMAL};
    const sca_rgf_t I2C_M_7B_W  {0xda, 1, 2, BO_NORMAL};
    const sca_rgf_t I2C_M_7B_R  {0xde, 1, 2, BO_NORMAL};
    const sca_rgf_t I2C_S_10B_W {0x8a, 2, 2, BO_NORMAL};
    const sca_rgf_t I2C_S_10B_R {0x8e, 2, 2, BO_NORMAL};
    const sca_rgf_t I2C_M_10B_W {0xe2, 2, 2, BO_NORMAL};
    const sca_rgf_t I2C_M_10B_R {0xe6, 2, 2, BO_NORMAL};
};



//------------------------------------------------------------------------------
/** Class for I2C communication with the GBTx via GBT SCA
 *  (7B addressing)
 */
class ScaI2cGbtx : public ScaI2c
{
  public:
    ScaI2cGbtx(ScaComm& sca_comm, uint8_t i2c_if_idx = 0, uint32_t slave_i2c_addr = 0) :
        ScaI2c(sca_comm, i2c_if_idx, slave_i2c_addr)
      {}

   uint8_t readByte(uint32_t daddr, uint32_t raddr) const override;
   uint8_t writeByte(uint32_t daddr, uint32_t raddr, uint8_t value) const override;
};



//------------------------------------------------------------------------------
/** Class for I2C communication with the SAMPAs via GBT SCA
 *  (10B addressing)
 */
class ScaI2cSampa : public ScaI2c
{
  private:
   /** Convert device addresses to D0/D1 data field contents
    *  @param daddr Slave device address
    *  @param raddr Slave register address
    *  @return Value of the 'D1[7..0] & D0[7..0]' field as '[15..8] & [7..0]'
    */
   inline uint16_t addressToD1D0(uint32_t daddr, uint32_t raddr) const
     {
       return static_cast<uint16_t>(
              (((daddr & 0x3) << 14) | ((raddr & 0x3f) << 8)) | // D1
               (0x78 | ((daddr >> 2) & 0x3))                    // D0
              );
     }

  public:
    ScaI2cSampa(ScaComm& sca_comm, uint8_t i2c_if_idx = 0, uint32_t slave_i2c_addr = 0) :
        ScaI2c(sca_comm, i2c_if_idx, slave_i2c_addr)
      {}

   uint8_t readByte(uint32_t daddr, uint32_t raddr) const override;
   uint8_t writeByte(uint32_t daddr, uint32_t raddr, uint8_t value) const override;
};

} // namespace gbt
