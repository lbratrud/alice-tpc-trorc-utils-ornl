/**
 *  gbtlink.hpp
 *  Copyright (C) 2016 Heiko Engel <hengel@cern.ch>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 **/
#pragma once

#include <librorc.h>
#include "trorc_registers.h"
#include "trorc.hpp"

namespace trorc {

class GbtLink
{
  private:
    const uint32_t base_addr_;
    const uint32_t base_addr_tx_;
    const uint32_t base_addr_rx_;

    //librorc::bar& bar_;
    Bar& bar_;

    std::unique_ptr<librorc::link> link_;
    std::unique_ptr<librorc::gtx> gtx_;

  public:
    GbtLink(Bar& bar, uint32_t link_number = 0) :
        base_addr_((link_number + 1) * RORC_CHANNEL_OFFSET),
        base_addr_tx_(base_addr_ + (1 << TRORC_REGFILE_GTXTX_SEL)),
        base_addr_rx_(base_addr_ + (1 << TRORC_REGFILE_GTXRX_SEL)),
        bar_(bar),
        link_(make_unique<librorc::link>(bar_.get(), link_number)),
        gtx_(make_unique<librorc::gtx>(link_.get()))
      {}

    /**
     * read SC register from dmaregfile
     * @param addr register address in dmaregfile
     * @return data at give address
     **/
    uint32_t pciReg(uint32_t addr) const;

    /**
     * write to SC register in dmaregfile
     * @addr register address in dmaregfile
     * @data data to be written
     **/
    void setPciReg(uint32_t addr, uint32_t data) const;

    /**
     * read SC register from gbt_rxfrmclk_regfile
     * @param addr register address in gbt_rxfrmclk_regfile
     * @return data at give address
     **/
    uint32_t gbtRxReg(uint32_t addr) const;

    /**
     * write to SC register in gbt_rxfrmclk_regfile
     * @addr register address in gbt_rxfrmclk_regfile
     * @data data to be written
     **/
    void setGbtRxReg(uint32_t addr, uint32_t data) const;

    /**
     * read SC register from gbt_txfrmclk_regfile
     * @param addr register address in gbt_txfrmclk_regfile
     * @return data at give address
     **/
    uint32_t gbtTxReg(uint32_t addr) const;

    /**
     * write to SC register in gbt_txfrmclk_regfile
     * @addr register address in gbt_txfrmclk_regfile
     * @data data to be written
     **/
    void setGbtTxReg(uint32_t addr, uint32_t data) const;

    /**
     * set GBT reset
     * @param val reset value, 0 or 1
     **/
    void setReset(uint32_t val) const;

    /**
     * get GBT reset state
     * @return 1 if in reset, 0 otherwise
     **/
    uint32_t getReset() const;

    /**
     * check if RX Frame Clock is Ready
     * @return 1 if ready, 0 otherwise
     **/
    uint32_t rxFrameClockReady() const;

    /**
     * check if TX Frame Clock is Ready
     * @return 1 if ready, 0 otherwise
     **/
    uint32_t txFrameClockReady() const;

    /**
     * check if GBT_RX_READY is active
     * @return 1 if RX_READT is active, 0 otherwise
     **/
    uint32_t rxReady() const;

    /**
     * wait for RX_READY to assert
     * @return 0 if RX_READY is active, -1 if RX_READY did not become active
     * within 5 seconds.
     **/
    int waitForRxReady() const;

    /**
     * clear the RX_READY-lost counter
     **/
    void clearRxReadyLostCount() const;

    /**
     * get the current state of the RX_READY-lost counter. Note: the counter does
     * not clear itself with a GBT reset.
     * @return number of RX_READY loss counts since the counter was cleared the
     * last time
     **/
    uint32_t rxReadyLostCount() const;

    /**
     * Set RX polarity of the GTX
     * @param val 0 for regular, 1 for inverted polarity
     **/
    void setRxPolarity(uint32_t val) const;

    /**
     * Set TX polarity of the GTX
     * @param val 0 for regular, 1 for inverted polarity
     **/
    void setTxPolarity(uint32_t val) const;

    /**
     * Get current RX pattern checker mode
     * @return 0: e-links/disabled, 1: static pattern, 2: counter pattern, 3: prbs
     * pattern
     **/
    uint32_t patternCheckerMode() const;

    /**
     * Set RX pattern checker mode
     * @param mode 0: e-links/disabled, 1: static pattern, 2: counter pattern, 3:
     * prbs pattern
     **/
    void setPatternCheckerMode(uint32_t mode) const;

    /**
     * Get current RX pattern checker widebus mode
     * @return 1 if expecting widebus data, 0 if expecting standard data
     **/
    uint32_t patternCheckerWidebus() const;

    /**
     * Set RX pattern checker widebus mode
     * @param mode 1 for widebus, 0 for standard mode
     **/
    void setPatternCheckerWidebus(uint32_t mode) const;

    /**
     * get current pattern checker error count
     * @return number of pattern checker errors
     **/
    uint32_t patternCheckerErrorCount() const;

    /**
     * clear pattern checker error count
     **/
    void clearPatternCheckerErrorCount() const;

    /**
     * set TX Idle Pattern
     * @param highdw bit16 is frame-vld flag, bits [15:0] contain txframe[79:64]
     * @param middw contains txframe[63:32]
     * @param lowdw contains txframe[31:0]
     **/
    void setTxIdlePattern(uint32_t highdw, uint32_t middw, uint32_t lowdw) const;
    uint32_t txIdlePatternHigh() const;
    uint32_t txIdlePatternMid() const;
    uint32_t txIdlePatternLow() const;

    /**
     * set TX Control Pattern
     * @param highdw bit16 is frame-vld flag, bits [15:0] contain txframe[79:64]
     * @param middw contains txframe[63:32]
     * @param lowdw contains txframe[31:0]
     **/
    void setTxControlPattern(uint32_t highdw, uint32_t middw, uint32_t lowdw) const;
    uint32_t txControlPatternHigh() const;
    uint32_t txControlPatternMid() const;
    uint32_t txControlPatternLow() const;

    /**
     * Get the number of cycles the control pattern will be driven
     * @return number of clock cycles
     **/
    uint32_t txControlPatternCycles() const;

    /**
     * Set control pattern cycles
     * @param cycles Number of cycles the control pattern is sent
     * @return Value read back
     */
    uint32_t setTxControlPatternCycles(uint32_t cycles) const;

    /**
     * drive the control pattern for a given amount of clock cycles
     * @param trigger_readout if set true the control pattern also triggers a
     * readout on the RX path
     **/
    void driveControlPattern(bool trigger_readout) const;

    /**
     * trigger the sending of the control pattern without starting a readout
     * process
     **/
    void triggerControlPattern() const;

    /**
     * check if control pattern is currently active
     * @return 1 if active, 0 otherwise
    **/
    uint32_t controlPatternActive() const;

    /**
     * stop sending the control pattern
     **/
    void disableControlPattern() const;

    /**
     * trigger a readout process
     **/
    void triggerReadout() const;

    /**
     * set number of frames that should be read out on a readout trigger
     * @param framecount number of frames to read out
     **/
    void setRxReadoutTargetFrameCount(uint32_t framecount) const;

    /**
     * get the number of frames to be read out on a readout trigger
     * @return number of frames to read out
     **/
    uint32_t rxReadoutTargetFrameCount() const;

    /**
     * get the current number of frames read out
     * @return number of frames read out
     **/
    uint32_t rxReadoutCurrentFrameCount() const;

    /**
     * Clear the current number of frames read out
     **/
    void clearRxReadoutCurrentFrameCount() const;

    /**
     * set the number of frames that should be packed into one event
     * @param framecount number of frames per event
     **/
    void setRxEventSizeFrameCount(uint32_t framecount) const;

    /**
     * get the number of frames that should be packed into one event
     * @return number of frames per event
     **/
    uint32_t rxEventSizeFrameCount() const;

    /**
     * stop the readout process if ongoing
     **/
    void stopReadout() const;

    /**
     * get numer of times the control pattern was activated
     * @return number of times the control pattern was activated
     **/
    uint32_t controlPatternStartCount() const;

    /**
     * clear the control pattern start counter
     **/
    void clearControlPatternStartCount() const;

    /**
     * check if external triggers are enabled
     * @return 1 if enabled, 0 if disbaled
     **/
    uint32_t allowExternalTrigger() const;

    /**
     * enable/disable external triggers
     * @param enable 1 to enable, 0 to disable
     **/
    void setAllowExternalTrigger(uint32_t enable) const;

    /**
    * get current software trigger channel mask. Returns a bit mask with one bit
    * for each GBT link in the firmware. A 1 means that the given link is read
    * out when a software trigger is sent from this link.
    * @return bit mask of links that are triggered from a software trigger on
    * this link.
    **/
    uint16_t softwareTriggerChannelMask() const;

    /**
     * set software trigger channel mask
     * @param mask bit mask of GBT links in the firmware that are read out with
     * a software trigger sent from this link.
     **/
    void setSoftwareTriggerChannelMask(uint16_t mask) const;

    /**
     * get current TX pattern channel mask. Returns a bit mask with one bit for
     * each GBT link in the firmware. A 1 means the TX control pattern is also
     * activated on that link if the control pattern is started on this link.
     * @return bit mask of links that also activate the control pattern on a
     * control pattern enable on this link.
     **/
    uint16_t txPatternChannelMask() const;

    /**
     * Set the TX pattern channel mask.
     * @param mask bit mask of GBT links in the firmware that also send the
     *control pattern if it is enabled for this link.
     **/
    void setTxPatternChannelMask(uint16_t mask) const;

    /**
     * get total number of triggers received on this link
     * @return number of triggers
     **/
    uint32_t triggerCount() const;

    /**
     * clear trigger counter
     **/
    void clearTriggerCount() const;

  /**
   * get error flag for 'control pattern start while active'
   * @return 1 if set, else '0'
   **/
  uint32_t controlPatternStartWhileActiveFlag() const;

  /**
   * clear error flag for 'control pattern start while active'
   **/
  void clearControlPatternStartWhileActiveFlag() const;

  /**
   * get error flag for 'trigger while busy'
   * @return 1 if set, else '0'
   **/
  uint32_t triggerWhileBusyFlag() const;

  /**
   * clear error flag for 'trigger while busy'
   **/
  void clearTriggerWhileBusyFlag() const;

  /**
   * get error flag for 'FIFO write error'
   * @return 1 if set, else '0'
   **/
  uint32_t fifoWriteErrorFlag() const;

  /**
   * clear error flag for 'FIFO write errors'
   **/
  void clearFifoWriteErrorFlag() const;

  /**
   * clear all error flags
   **/
  void clearErrorFlags() const;

  /**
   * ADC clock error counter
   * @param sampa ADC clock of this SAMPA
   * @return number of errors
   **/
  uint32_t decAdcErrorCounter(uint32_t sampa) const;

  /**
   * SyncPattern counter
   * @param halfSampa syncPattern counter if this half SAMPA
   * @return number of detected syncPattern
   **/
  uint32_t decSyncPatternCounter(uint32_t halfSampa) const;

  /**
   * ID error counter
   * @param sampa ID missmatch of this SAMPA
   * @return number of missmatches
   **/
  uint32_t decIdError(uint32_t sampa) const;

  /**
   * ADC clock status
   * @return status of all 3 ADC clocks
   **/
  uint32_t decAdcStatus() const;

  /**
   * ADC SyncPattern status
   * @return status of all 5 sync patterns
   **/
  uint32_t decSyncPatternStatus() const;

  /**
   * Set T-RORC readout mode
   * @param mode 0: readout disabled -> no data, 1: raw GBT frames, 
   * 2: decoded data, 3: raw GBT frames + decoded data
   **/
  void setReadoutMode(uint32_t mode) const;

  /**
   * Get T-RORC readout mode
   * @return mode 0: readout disabled -> no data, 1: raw GBT frames, 
   * 2: decoded data, 3: raw GBT frames + decoded data
   **/
  uint32_t getReadoutMode() const;

  /**
   * Resets several counter of the decoder
   * @param cnt bit 0: Adc clock monitor, bit 1: Sync pattern, bit 2: ID error
   **/
  void resetDecoderCounter(uint32_t cnt) const;

  /**
   * Resets Decoder and disable/enable
   * @param param bit 0: enable/disable decoder, bit 1: reset decoder,
   * bit 2: reset adc clock monitor, bit 3 reset channel extractor
   **/
  void setDecoder(uint32_t param) const;

  /**
   * Decoder status
   * @return status of Decoder
   **/
  bool getDecoderStatus() const;

};

}  // namespace trorc
