#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <vector>

#include "trorc.hpp"
#include "gbt_sca_comm.hpp"
#include "fec.hpp"
#include "git_info.hpp"

using namespace gbt;
namespace bpo = boost::program_options;

int main(int argc, char** argv)
{
  bpo::variables_map vm;
  bpo::options_description opt_general(
    "Usage:\n  " + std::string(argv[0]) + " <cmds/options>\n"
    "  Tool will apply the operations on all FECs/SCAs defined by the corresponding mask\n"
    "  Which SAMPAs on a given FEC are affected is controlled by the SAMPA mask\n"
    "Commands / Options");
  bpo::options_description opt_hidden("");
  bpo::options_description opt_all;
  bpo::positional_options_description opt_pos;

  try
  {
    opt_general.add_options()
      ("help,h", "Print this help message")
      ("version", "Print version information")
      ("verbose,v", "Be verbose")
      ("mask,m", bpo::value<cl_uint_t>()->default_value(0x1),"Mask of FECs/SCAs to apply commands below to")
      ("adctrim,a", bpo::value<cl_uint_t>()->default_value(0x4),"Set ADCTrim in SAMPAs")
      ("sampa-mask", bpo::value<cl_uint_t>()->default_value(0x1f), "0x1f") 
      ("scan", "Scan I2C for active (replying) SAMPAs")
      ("pwr-on", "Turn on selected SAMPAs")
      ("pwr-off", "Turn off selected SAMPAs")
      ("elinks", bpo::value<uint32_t>(), "Enable n SAMPA elinks")
//      ("write,w", bpo::value<std::vector<cl_uint_t>>()->multitoken(), "Write to SAMPA registers, input is expected as 'addr value' pairs")
      ;
    opt_all.add(opt_general).add(opt_hidden);

    bpo::store(bpo::command_line_parser(argc, argv).options(opt_all)
                     .positional(opt_pos).run(), vm);

    if (vm.count("help") || argc == 1) {
      std::cout << opt_general << std::endl;
      exit(0);
    }

    bpo::notify(vm);

    // This is a bit of a hack, it would be better to create a type compatible with
    //   program_options that always consumes exactly 2 cl_uint_t values
    if (vm.count("write")) {
      const std::vector<cl_uint_t>& w = vm["write"].as<std::vector<cl_uint_t>>();
      if (w.size() % 2)
        throw bpo::error("--write : Uneven number of values, cannot interpret them as 'addr value' pairs");
    }
  }
  catch(bpo::error& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << opt_general << std::endl;
    exit(1);
  }
  catch(std::exception& e)
  {
    std::cerr << e.what() << ", application will now exit" << std::endl;
    exit(2);
  }

  // Do stuff
  int device_number(0);
  int bar_number(1);
  std::unique_ptr<trorc::Device> trorc;
  std::unique_ptr<trorc::Bar> bar;

  try {
    trorc.reset(new trorc::Device(device_number));
    bar.reset(new trorc::Bar(*trorc, bar_number));
  } catch (int e) {
    std::cerr << "ERROR: Failed to initialize T-RORC: " << librorc::errMsg(e)
              << std::endl;
    exit(1);
  }

  if (vm.count("version")) {
    std::cout << GitInfo();
  }


  try {
    // FEC, or SCA loop
    for (uint32_t fec_idx = 0; fec_idx < 32; fec_idx++) {
      // Specified FECs/SCAs
      if ((vm["mask"].as<cl_uint_t>()() >> fec_idx) & 0x1) {

        GscTrorcHdlcSw sca_comm(*bar, fec_idx, true, false);
        Fec fec(sca_comm);

        if (vm.count("verbose"))
          std::cout << "Communcating with SCA" << fec_idx << ". Sampa mask is 0x"
                    << std::hex << vm["sampa-mask"].as<cl_uint_t>()() << std::dec << std::endl;

        if (vm.count("scan")) {
          fec.printIdString(std::cout);
          std::cout << "Scanning SAMPA I2C ports for active devices" << std::endl;
          fec.sampaI2cScan(&std::cout);
        }

        if (vm.count("pwr-off")) {
          std::cout << "Powering off SAMPAs " << std::endl;
          fec.sampaPwrOff(vm["sampa-mask"].as<cl_uint_t>()() & 0xff);
        }
        else if (vm.count("pwr-on")) {
          std::cout << "Powering on SAMPAs " << std::endl;
          fec.sampaPwrOn(vm["sampa-mask"].as<cl_uint_t>()() & 0xff);
        }
	int adctrim = vm["adctrim"].as<cl_uint_t>()() & 0x7;
//        std::cout << "ADCtrim= " << std::hex << adctrim << "<|" << std::endl;
        fec.sampaAdcTrim(adctrim);
        fec.sampaReadClkConf(vm["sampa-mask"].as<cl_uint_t>()() & 0xff);

        if (vm.count("elinks")) {
          std::cout << "Enabling SAMPA elinks" << std::endl;
          fec.sampaEnableElinks(vm["elinks"].as<uint32_t>() & 0xff, vm["sampa-mask"].as<cl_uint_t>()() & 0xff);
          //fec.sampaWriteReg(0x31, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x0D);
          //fec.sampaWriteReg(0x1b, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x12);
          //fec.sampaWriteReg(0x3b, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x12);
	  //fec.sampaWriteReg(0x31, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x0d);
	  //fec.sampaWriteReg(0x1b, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x12);
	 /*
	  fec.sampaWriteReg(0x0, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x24);
	  fec.sampaWriteReg(0x0, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x25);
	  fec.sampaWriteReg(0x0, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x26);
	  fec.sampaWriteReg(0x0, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x27);
	  */
          /*
	  fec.sampaWriteReg(0x2, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x0E);
          fec.sampaWriteReg(0x3, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x0E);
          fec.sampaWriteReg(0x4, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x0E);
          fec.sampaWriteReg(0x5, vm["sampa-mask"].as<cl_uint_t>()() & 0xff,0x0E);
	  */
        }
        fec.sampaReadRegs(vm["sampa-mask"].as<cl_uint_t>()() & 0xff);

        if (vm.count("write")) {
          // See above: We (can) assume here, that vector always contains even number n of values, which
          //   are interpreted as 'addr_0 value_0 ... addr_n/2 value_n/2' pairs
          const std::vector<cl_uint_t>& w = vm["write"].as<std::vector<cl_uint_t>>();

          for (uint32_t i = 0; i < w.size()/2; i++) {
            uint32_t addr = w[i]();
            uint32_t val  = w[i+1]();
            std::cout << i << " -> " << addr << " | " << val << "\n";
          }
        }
	std::cout << fec << "\n";
      }   // Specified SCAs
    }   // SCA loop
  }
  catch (std::runtime_error& e) {
    std::cerr << e.what() << std::endl;
    exit(100);
  }

  return 0;
}
