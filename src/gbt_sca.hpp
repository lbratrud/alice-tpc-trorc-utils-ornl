#pragma once
#include <tuple>

#include "gbt_sca_comm.hpp"
#include "hdlc.hpp"

#ifdef SCA_DEBUG
  #define scadebug(s) std::cout << __func__ << " : " << s << std::endl;
  #define adcdebug(s) std::cout << s << std::endl;
#else
  #define scadebug(...)
  #define adcdebug(...)
#endif

namespace gbt {

//------------------------------------------------------------------------------
/** SCA Exception class
 */
class ScaException : public std::runtime_error
{
  public:
    ScaException(const std::string& s, uint8_t channel) :
      std::runtime_error("ERROR : SCA [CH" + std::to_string(channel) + "] : " + s) {}
};



//------------------------------------------------------------------------------
/** GBT SCA common features
 */
class Sca {
  protected:
    ScaComm& sca_comm_;
    uint8_t channel_;
    
    /** Reverse byte order
     *  @param a[31..0] input value
     *  @return a[7..0, ..., 31..24]
     */
    inline uint32_t rbo(uint32_t a) const
      {
        return ((a & 0xff) << 24) | ((a & 0xff00) << 8) |
               ((a & 0xff0000) >> 8) | ((a & 0xff000000) >> 24);
      }

    /** Reverse halfword order
     *  @param a[31..0] input value
     *  @return a[15..8, 7..0, 31..24, 23..16]
     */
    inline uint32_t rhwo(uint32_t a) const
      {
        return ((a & 0xffff) << 16) | ((a & 0xffff0000) >> 16);
      }

  public:
    Sca(ScaComm& sc, uint8_t channel) : sca_comm_(sc), channel_(channel) {}

    /** Get the SCA channel information
     *  @return Currently configured SCA channel
     */
    uint8_t getChannel() const { return channel_; }

    /** Type to specify the byte order definition for SCA transceives
     *    - BO_NORMAL [31..0]
     *    - BO_REVERSED [7..0, 15..8, 23..16, 31..24]
     *    - BO_REVERSED_HW [15..8, 7..0, 31..24, 23..16]
     */
    enum sca_byteorder_t : uint8_t { BO_NORMAL, BO_REVERSED, BO_REVERSED_HW };

    /** Tuple for the SCA register file information\n
     *    - <0> TX command
     *    - <1> TX length
     *    - <2> RX length
     *    - <3> Byte order: BO_NORMAL, BO_REVERSED, BO_REVERSED_HW
     */
    using sca_rgf_t = std::tuple<uint8_t, uint8_t, uint8_t, sca_byteorder_t>;

    /** Send and receive packet to/from SCA
     *
     *  @note Sending and receiving is raw, not length masking of received data or
     *        automatic bit(re-)ordering is applied at this stage
     *  @param request Input frame payload
     *  @return Payload of the received frame
     */
    inline HdlcEcFramePayload transceive(HdlcEcFramePayload& request) const
      {
        HdlcEcFramePayload reply;
        sca_comm_.execCommand(request, reply);
        return reply;
      }

    /** Send and receive packet to/from SCA
     *
     *  Uses the byte ordering information from the register file information to
     *    potentially apply byte reordering to the Tx and Rx data\n
     *  Uses the Rx length from the register file information to apply
     *    a corresponding mask on the received input data
     *
     *  @param rgf SCA register file definition (command and length part of the request)
     *  @return Payload of the received frame with potentially reordered and masked data field
     */
    inline HdlcEcFramePayload transceive(const sca_rgf_t& rgf, const uint32_t data = 0x0) const
      {
        uint32_t wdata;
        switch(std::get<3>(rgf)) {
          case BO_REVERSED:    wdata = rbo(data); break;
          case BO_REVERSED_HW: wdata = rhwo(data); break;
          case BO_NORMAL:
          default: wdata = data; break;
        }

        HdlcEcFramePayload request{0x0, channel_, std::get<1>(rgf), std::get<0>(rgf), wdata};
        HdlcEcFramePayload reply = transceive(request);

        switch(std::get<3>(rgf)) {
          case BO_REVERSED:    reply.data = rbo(reply.data);  break;
          case BO_REVERSED_HW: reply.data = rhwo(reply.data); break;
          case BO_NORMAL:
          default: break;
        }
        reply.data &= ((std::get<2>(rgf) == 4) ? 0xffffffff :
                                                 ((uint32_t(1) << std::get<2>(rgf) * 8) - 1));
        return reply;
      }
};



//------------------------------------------------------------------------------
/** GBT SCA ADC interfacing
 */
class ScaAdc : public Sca
{
  private:
    /** Get input select MUX setting
     */
    int version=2;
    uint8_t getInputSelection() const { 
	if (version==2) return transceive(CMD_R_INSEL).data;
	return transceive(CMD_R_INSEL).data;
       }
    /** Control the input selection MUX
     */
    uint8_t setInputSelection(uint8_t idx) const
      {
	if (version==2)
	{
        	transceive(CMD_W_MUX, static_cast<uint32_t>(idx) & 0x1f);
	}
	else
	{
        	transceive(CMD_W_INSEL, static_cast<uint32_t>(idx) & 0x1f);
	}
        return getInputSelection();
      }

    /** Get the currently enabled current source
     *  @return Read-back current enable register value
     */
    uint32_t getCurrentSource() const {
	if (version==2) return transceive(CMD_R_CURR).data;
	return transceive(CMD_R_CUREN).data;
      }

    /** Set the current source enable
     *  @param idx ADC input pin index
     *  @return Read-back current enable register value
     *  @note Ensures that only one bit of CUREN is active at any time
     */
    uint32_t setCurrentSource(uint8_t idx) const
      {
	if (version==2)
	{
        	transceive(CMD_W_CURR, (uint32_t(1) << idx));
	}
	else
	{
        	transceive(CMD_W_CUREN, (uint32_t(1) << idx));
	}
        return getCurrentSource();
      }

    /** Not documented in GBT SCA user manual */
    uint32_t getData() const { return transceive(CMD_R_DATA).data; }

    /** Not documented in GBT SCA user manual */
    uint32_t getRawData() const { return transceive(CMD_R_RAW).data; }

    /* ADC register file information, from SCA manual v8.0 p60ff */
    const sca_rgf_t CMD_R_ID       {0xD1, 1, 4, BO_REVERSED};
    const sca_rgf_t CMD_R_IDV1     {0x91, 1, 4, BO_REVERSED};
    const sca_rgf_t CMD_W_INSEL    {0x30, 4, 2, BO_REVERSED};
    const sca_rgf_t CMD_R_INSEL    {0x31, 1, 4, BO_REVERSED};
    const sca_rgf_t CMD_W_MUX      {0x50, 4, 2, BO_REVERSED};
    const sca_rgf_t CMD_R_MUX      {0x51, 1, 4, BO_REVERSED};
    const sca_rgf_t CMD_W_CUREN    {0x40, 4, 2, BO_REVERSED};
    const sca_rgf_t CMD_R_CUREN    {0x41, 1, 4, BO_REVERSED};
    const sca_rgf_t CMD_W_CURR     {0x60, 4, 2, BO_REVERSED};
    const sca_rgf_t CMD_R_CURR     {0x61, 1, 4, BO_REVERSED};
    const float input_range_{1.0};

  public:
    ScaAdc(ScaComm& sca_comm) : Sca(sca_comm, 0x14) {}

    /** Get gain calibration value
     *  @return Read-back gain calibration value
     */
    uint32_t getGainCalib() const { return transceive(CMD_R_GAINCALIB).data; }
    uint32_t getGain() const { return transceive(CMD_R_GAIN).data; }
    uint32_t getID() const {
	 if (version==2) return transceive(CMD_R_ID).data;
	 return transceive(CMD_R_IDV1).data;
     }
    uint32_t getReg(uint8_t idx) const {
    	 sca_rgf_t CMD_R_REGISTER = std::make_tuple(idx, 1, 4, BO_REVERSED);
	 return transceive(CMD_R_REGISTER).data;
     }

    /** Set the gain calibration value
     *  @param Value to set
     *  @return Read-back gain calibration register value
     */
    uint32_t setGain(uint32_t value) const
      {
        transceive(CMD_W_GAIN, value);
        return getGain();
      }
    uint32_t setGainCalib(uint32_t value) const
      {
        transceive(CMD_W_GAINCALIB, value);
        return getGainCalib();
      }

    /** Disable all current sources
     */
    void disableCurrentSources() const { transceive(CMD_W_CUREN, 0x0); }

    /** Sample ADC input
     *  @param idx Index of the input to sample
     *  @param enableCs Enable corresponding current source
     *  @return 0xffff in case of illegal input idx, or, in case of successful conversion:\n
     *            - [11..0] Conversion result
     *            - [12]    Voltage exceeded during conversion flag
     *            - [15..13] Zero
     */
    uint16_t sample(uint8_t idx, bool enableCs = false) const
      {
        uint16_t v(0xffff);
        if (idx >= 0x0 && idx <= 0x1f) {
          setInputSelection(idx);
          if (enableCs) {
            setCurrentSource(idx);
          }
	  if (version==2)
	  {
              v = transceive(CMD_W_GO,1).data;
	  }
	  else
	  {
              transceive(CMD_W_GO_V1);
              v = transceive(CMD_R_RAW_V1).data;
	  }
	  //vn = (v & 0xff) << 8 | (v >> 8);
          //v= vn;
          adcdebug(__func__ << std::hex
                            << " : INSEL = 0x" << static_cast<uint32_t>(getInputSelection())
                            << " / CUREN = 0x" << getCurrentSource()
                            << " / SAMPLE = 0x" << static_cast<uint32_t>(v) << std::dec)
          if (enableCs) {
            disableCurrentSources();
          }
        }
        return v;
      }

    /** Dump ADC registers
     */
    friend std::ostream& operator<<(std::ostream& os, const ScaAdc& f)
      {
        os << "GBT-SCA ADC status\n"
           << std::hex << std::right << std::setfill('0')
           << "  GAINCALIB 0x" << std::setw(4) << f.getGainCalib() << "\n"
           << "  GAIN 0x" << std::setw(4) << f.getGain() << "\n"
           << "  CUREN     0x" << std::setw(8) << f.getCurrentSource() << "\n"
           << "  SCAID     0x" << std::setw(8) << f.getID() << "\n"
           << std::dec << std::left << std::setfill(' ')
           << "  INSEL     " << std::hex << static_cast<uint32_t>(f.getInputSelection()) << "\n"
           << "";
        return os;
      }

    /* ADC register file information, from SCA manual v8.0 p60ff */
    const sca_rgf_t CMD_W_GO_V1    	{0xb2, 4, 2, BO_REVERSED};
    const sca_rgf_t CMD_R_RAW_V1   	{0x51, 1, 4, BO_REVERSED};

// Both
    const sca_rgf_t CMD_W_GO       	{0x2,  1, 2, BO_REVERSED};
    const sca_rgf_t CMD_R_DATA     	{0x21, 1, 2, BO_REVERSED};
    const sca_rgf_t CMD_R_RAW      	{0x31, 1, 2, BO_REVERSED};
// V1
    const sca_rgf_t CMD_W_GAINCALIB	{0x70, 2, 1, BO_REVERSED};
    const sca_rgf_t CMD_R_GAINCALIB	{0x71, 1, 2, BO_REVERSED};
// V2
    const sca_rgf_t CMD_W_GAIN		{0x10, 2, 1, BO_REVERSED};
    const sca_rgf_t CMD_R_GAIN		{0x11, 1, 2, BO_REVERSED};
};


//------------------------------------------------------------------------------
/** GBT SCA GPIO interfacing
 */
class ScaGpio : public Sca
{
  public:
    ScaGpio(ScaComm& sca_comm) : Sca(sca_comm, 0x2) {}

    /** Get GPIO direction
     *  @return Current DIRECTION value
     */
    uint32_t getDirection() const { return transceive(CMD_R_DIRECTION).data; }

    uint32_t getReg(uint8_t idx) const {
    	 sca_rgf_t CMD_R_REGISTER = std::make_tuple(idx, 1, 4, BO_REVERSED);
	 return transceive(CMD_R_REGISTER).data;
     }
    /** Set GPIO direction
     *  @param value Desired value of the DIRECTION register
     *  @return Current DIRECTION value
     */
    uint32_t setDirection(uint32_t value) const
      {
        transceive(CMD_W_DIRECTION, value);
        return getDirection();
      }

    /** Read register DATAIN
     *  @return Current DATAIN value
     */
    uint32_t getDataIn() const { return transceive(CMD_R_DATAIN).data; }

    /** Read register DATAOUT
     *  @return Current DATAOUT value
     */
    uint32_t getDataOut() const { return transceive(CMD_R_DATAOUT).data; }

    /** Write register DATAOUT
     *  @param value Desired value of the DATAOUT register
     *  @return Current DATAOUT value
     */
    uint32_t setDataOut(uint32_t value) const
      {
        transceive(CMD_W_DATAOUT, value);
        return getDataOut();
      }

    /** Dump GPIO registers
     */
    friend std::ostream& operator<<(std::ostream& os, const ScaGpio& f)
      {
        os << "GBT-SCA GPIO status\n"
           << std::hex << std::right << std::setfill('0')
           << "  DIR  0x" << std::setw(8) << f.getDirection() << "\n"
           << "  DIN  0x" << std::setw(8) << f.getDataIn() << "\n"
           << "  DOUT 0x" << std::setw(8) << f.getDataOut() << "\n"
           << std::dec << std::left << std::setfill('0');
        return os;
      }

    /* GPIO register file information, from GBT SCA manual v8.0 p51ff */
    const sca_rgf_t CMD_R_DATAIN    {0x01, 1, 4, BO_REVERSED};
    const sca_rgf_t CMD_W_DATAOUT   {0x10, 4, 2, BO_REVERSED};
    const sca_rgf_t CMD_R_DATAOUT   {0x11, 1, 4, BO_REVERSED};
    const sca_rgf_t CMD_W_DIRECTION {0x20, 4, 2, BO_REVERSED};
    const sca_rgf_t CMD_R_DIRECTION {0x21, 1, 4, BO_REVERSED};
};



//------------------------------------------------------------------------------
/** GBT SCA basic configuration
 */
class ScaBasic : public Sca
{
  public:
    ScaBasic(ScaComm& sca_comm) : Sca(sca_comm, 0x0) {}

    friend std::ostream& operator<<(std::ostream& os, const ScaBasic& f)
      {
        os << "GBT-SCA basic configuration\n"
           << std::hex << std::right << std::setfill('0')
           << "  CRB 0x" << std::setw(8) << ((f.transceive(f.CMD_R_CRB)).data) << "\n"
           << "  CRC 0x" << std::setw(8) << ((f.transceive(f.CMD_R_CRC)).data) << "\n"
           << "  CRD 0x" << std::setw(8) << ((f.transceive(f.CMD_R_CRD)).data) << "\n"
           << std::dec << std::left << std::setfill('0');
        return os; 
      }

    const sca_rgf_t CMD_W_CRA {0x0, 1, 1, BO_NORMAL};
    const sca_rgf_t CMD_R_CRA {0x1, 1, 1, BO_NORMAL};
    const sca_rgf_t CMD_W_CRB {0x2, 1, 1, BO_NORMAL};
    const sca_rgf_t CMD_R_CRB {0x3, 1, 1, BO_NORMAL};
    const sca_rgf_t CMD_W_CRC {0x4, 1, 1, BO_NORMAL};
    const sca_rgf_t CMD_R_CRC {0x5, 1, 1, BO_NORMAL};
    const sca_rgf_t CMD_W_CRD {0x6, 1, 1, BO_NORMAL};
    const sca_rgf_t CMD_R_CRD {0x7, 1, 1, BO_NORMAL};
};

} // namespace gbt

#undef scadebug
#undef adcdebug
