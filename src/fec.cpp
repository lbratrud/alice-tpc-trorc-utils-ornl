#include "fec.hpp"

//------------------------------------------------------------------------------
void Fec::init(int CG0,int CG1,int CTS,int POL, int init,int V2) const
{
   uint32_t config;
   // Set basic GBT SCA registers (CRB, CRC, CRD)
   std::cout << "Configuring GBT SCA basic registers init=" << std::hex << init << "\n";
   sca_basic_->transceive(sca_basic_->CMD_W_CRB, cfg_basic_crb_);
   sca_basic_->transceive(sca_basic_->CMD_W_CRC, cfg_basic_crc_);
   sca_basic_->transceive(sca_basic_->CMD_W_CRD, cfg_basic_crd_ | 0xff);

   // Basic configuration for GPIO
   if (verbosity_ >= 1)
     std::cout << "Configuring GBT SCA GPIO\n";
    
   //config = 0x13*0x800000 + 0x80*(CTS&0x1)+0x100*POL;
   //config = 0x03*0x800000 + 0x80*(CTS&0x1)+0x100*POL;
   V2 = 0;
   if (V2!=0)
   {
      std::cout << "SAMPA V2 mode\n";
      config = 0x53*0x800000 + 0x80*(CTS&0x1)+0x100*POL;
   }
   else
   {
      config = 0x41*0x800000 + 0x80*(CTS&0x1)+0x100*POL;
   }
   //config = 0x01*0x800000 + 0x80*(CTS&0x1)+0x100*POL;
   //config = 0x53*0x800000 + 0x80*(CTS&0x1)+0x100*POL;
   //config = 0x29800000 + 0x80*(CTS&0x1)+0x100*POL;
   //config = 0x09800000 + 0x80*(CTS&0x1)+0x100*POL;
   std::cout << "config=0x" << std::hex << (int) config << "\n";
   if (init!=1)
   {
       config |= 0x1f; 
   }
   if (CG0 & 0x1)
   {
       config += 0x20;
       CG1 = 0;
   }
   else
   {
       if (CG1 & 0x1)
       {
           config += 0x40; 
       }
   }
   sca_gpio_->setDataOut(config);

   sca_gpio_->setDirection(cfg_gpio_direction_);
   // Configuration for ADC
   if (verbosity_ >= 1)
      std::cout << "Configuring GBT SCA ADC\n";
   sca_adc_->disableCurrentSources();
   sca_adc_->setGainCalib(cfg_adc_gaincalib_);
   std::cout << "SAMPA Configuration:\n    ";
   std::cout << "CG0=" << std::hex << CG0 << " ";
   std::cout << "CG1=" << std::hex << CG1 << " ";
   if (CG0==1)
   {
       std::cout << "Gain=30mV/fC";
   }
   else
   {
       if (CG1==1)
       {
           std::cout << "Gain=20mV/fC";
       }
       else
       {
           std::cout << "Gain=4mV/fC";
       }
   }
   std::cout << "\n    CTS=" << std::hex << CTS << " \n";
   std::cout << "\n    POL=" << std::hex << POL << " \n";
//   std::cout << "GainCalib " << std::hex << sca_adc_->getGainCalib() << " \n";

   if (verbosity_ >= 1)
     std::cout << "Configuring GBT SCA I2C interfaces\n";
   for (uint32_t i = 0; i < 2; i++)
     i2c_gbtx_[i]->setFreq();
   for (uint32_t i = 0; i < 5; i++)
     i2c_sampa_[i]->setFreq();
}


//------------------------------------------------------------------------------
void
Fec::gbtxConfigure(uint8_t gbt_idx, const std::string& cfg_file) const
{
  std::ifstream gcfg(cfg_file, std::ios::in);
  if (!gcfg.is_open())
    throw std::runtime_error("Unable to open file " + cfg_file);

  if (verbosity_ >= 1)
    std::cout << "Configuring GBTx" << static_cast<uint32_t>(gbt_idx)
              << " with file " << cfg_file << "\n";

  const gbt::ScaI2c& i2c(*i2c_gbtx_[gbt_idx]);

  std::string line;
  uint32_t addr(0);
  uint32_t val;
  while (gcfg.good()) {
    if (gcfg.peek() == '#') {
      getline(gcfg, line);
      continue;
    }
    gcfg >> std::hex >> val;
    if (!gcfg.good())
      break;
    i2c.writeByte(addr, val & 0xff);
    addr++;
  }

  gcfg.close();

  // Force 'config ready'
  uint32_t cfg_done = i2c.writeByte(365, 0xaa);
  if (verbosity_ >= 2)
    std::cout << "Forcing GBTx config done to 0x" << std::hex
              << cfg_done << std::dec << "\n";

  if (verbosity_ >= 2)
    std::cout << "Forcing GBTx reset\n";
  gbtxReset(gbt_idx);
}


void
Fec::gbtxDumpConfiguration(uint8_t gbt_idx, std::ostream& os) const
{
  const gbt::ScaI2c& i2c(*i2c_gbtx_[gbt_idx]);
  os << "#GBTx " << static_cast<uint32_t>(gbt_idx) << " configuration\n";
  for (uint32_t addr = 0; addr < 366; addr++)
    os << std::right << std::dec << std::setfill(' ') << std::setw(3)
       << addr << " : 0x"
       << std::hex << std::setfill('0') << std::setw(2)
       << static_cast<uint32_t>(i2c.readByte(addr)) << "\n";
}


void
Fec::gbtxReset(uint8_t gbt_idx) const
{
  const gbt::ScaI2c& i2c(*i2c_gbtx_[gbt_idx]);
  uint8_t r = i2c.readByte(50) & 0xc7;
  i2c.writeByte(50, r | 0x38);
  i2c.writeByte(50, r);
}


int32_t
Fec::gbtxI2cScan(uint8_t gbt_idx, std::ostream* const os) const
{
  int32_t addr(-1);
  for (uint8_t i = 1; i < 16; i++) {    // 0 is broadcast address
    try {
      const gbt::ScaI2c& gbt(*i2c_gbtx_[gbt_idx]);
      gbt.readByte(i, 62); // Phase aligner track mode register, but anything should do here
      if (os) {
        *os << "  Found GBTx" << static_cast<uint32_t>(gbt_idx)
            << " [I2C addr " << static_cast<uint32_t>(i)
            << ", SCA CH" << static_cast<uint32_t>(gbt.getChannel()) << "]\n";
      }
      addr = i;
    }
    catch (gbt::ScaException& e) { /* Ignore these here */ }
  }

  if ((addr == -1) && os)
    *os << "  Unable to detect GBTx" << static_cast<uint32_t>(gbt_idx) << " I2C\n";

  return addr;
}


//------------------------------------------------------------------------------
uint32_t
Fec::sampaPwrCtrl(uint32_t value, uint32_t mask) const
{
  uint32_t cval = sca_gpio_->getDataIn() & (0xffffffff ^ (mask & 0x1f));
  sca_gpio_->setDataOut(cval | (value & mask));
  return sca_gpio_->getDataIn() & 0x1f;
}


bool
Fec::sampaAdcTrim(uint8_t n, uint32_t sampa_mask) const
{
  uint8_t nchk(n);
  for (uint8_t i = 0; i < n_sampa_; i++) {
    if ((sampa_mask >> i) & 0x1)
      nchk &= i2c_sampa_[i]->writeByte(REG_SAMPA_ADCTRIM, n);
  }
  return (nchk == n);
}

bool
Fec::sampaEnableElinks(uint8_t n, uint32_t sampa_mask) const
{
  uint8_t nchk(n);
  for (uint8_t i = 0; i < n_sampa_; i++) {
    if ((sampa_mask >> i) & 0x1)
      nchk &= i2c_sampa_[i]->writeByte(REG_SAMPA_SOCFG, n);
  }
  return (nchk == n);
}


bool
Fec::sampaWriteReg(uint8_t n, uint32_t sampa_mask,uint8_t addr) const
{
  uint8_t nchk(n);
  //std::cout << "Reg[0x" << std::hex << (int) addr << "]= 0x" << std::hex << (int) n << std::endl;
  for (uint8_t i = 0; i < n_sampa_; i++) {
    if ((sampa_mask >> i) & 0x1)
      nchk &= i2c_sampa_[i]->writeByte(addr, n);
  }
  return (nchk == n);
}


bool
Fec::sampaReadClkConf(uint8_t n, uint32_t sampa_mask) const
{
  uint32_t ncnfg;
  for (uint8_t i = 0; i < n_sampa_; i++) {
    if ((sampa_mask >> i) & 0x1)
      ncnfg = i*2;
      ncnfg = i2c_sampa_[i]->readByte(REG_SAMPA_CLKCONF);
//      ncnfg = i2c_sampa_[i]->readByte(REG_SAMPA_SOCFG);
//      std::cout << "Clock Config["<< std::dec << (int) i;
//      std::cout << "]= 0x" << std::hex << (int) ncnfg << "\n";
  }
}

bool
Fec::sampaReadRegs(uint8_t n, uint32_t sampa_mask) const
{
  uint32_t ncnfg;
  for(uint8_t j = 0; j < 40;j ++)
    for (uint8_t i = 0; i < n_sampa_; i++) {
      if ((sampa_mask >> i) & 0x1)
      {
          ncnfg = i2c_sampa_[i]->readByte(j);
          //std::cout << "Reg["<< std::dec << (int) i;
          //std::cout << "][0x"<< std::hex << (int) j;
          //std::cout << "]= 0x" << std::hex << (int) ncnfg << "\n";
      }
  }
}


uint32_t
Fec::sampaI2cScan(std::ostream* const os) const
{
  uint32_t active_mask(0x0);
  for (uint8_t i = 0; i < n_sampa_; i++) {
    try {
      const gbt::ScaI2c& sampa(*i2c_sampa_[i]);
      sampa.readByte(0x7);
      if (os) {
        *os << "  Found SAMPA" << static_cast<uint32_t>(i)
            << " [I2C addr " << sampa.getSlaveAddress()
            << ", SCA CH" << static_cast<uint32_t>(sampa.getChannel()) << "]\n";
      }
      active_mask |= (1 << i);
    }
    catch(gbt::ScaException& e) { /* Ignore these here */ }
  }
  return active_mask;
}
