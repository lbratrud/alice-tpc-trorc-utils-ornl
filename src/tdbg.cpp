#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <vector>
#include <boost/program_options.hpp>

#include "trorc.hpp"
#include "gbt_link.hpp"
#include "gbt_sca_comm.hpp"
#include "fec.hpp"
#include "git_info.hpp"

using namespace gbt;
namespace bpo = boost::program_options;


void
test0(ScaComm& sca_comm)
{
  std::cout << "Running single HDLC frame test: GPIO direction READ" << std::endl;
  std::cout << "--------------------------------------------------------------------------------\n";
  HdlcEcFramePayload request = {0xab, 0x2, 0x4, 0x21, 0xaddeabab};    // GPIO Direction READ
  HdlcEcFramePayload reply;
  sca_comm.execCommand(request, reply);

  if (reply.error)
    std::cout << sca_comm.getPayloadErrorMessage(reply.error) << std::endl;
}

void
test1(ScaComm& sca_comm)
{
  std::cout << "Running single HDLC frame test: GPIO direction WRITE" << std::endl;
  std::cout << "--------------------------------------------------------------------------------\n";
  HdlcEcFramePayload request = {0xe, 0x2, 0x4, 0x20, 0x55555555};    // GPIO Direction WRITE
  HdlcEcFrame reply;
  // HdlcEcFramePayload reply;
  sca_comm.sendRequest(request);
  sleep(1);
  sca_comm.getFrame(reply);
//  sca_comm.execCommand(request, reply);
//
//  if (reply.error)
//    std::cout << sca_comm.getPayloadErrorMessage(reply.error) << std::endl;
}

void
test2(ScaComm& sca_comm)
{
  std::cout << "Running single HDLC frame test: ADC" << std::endl;
  std::cout << "--------------------------------------------------------------------------------\n";
  HdlcEcFramePayload request = {0xf, 0x14, 0x4, 0x2, 0x01010101};    // ADC Tx
  HdlcEcFramePayload reply;
  sca_comm.execCommand(request, reply);

  if (reply.error)
    std::cout << sca_comm.getPayloadErrorMessage(reply.error) << std::endl;
}

void
test3(ScaComm& sca_comm)
{
  std::cout << "Running multi HDLC frame test" << std::endl;
  std::cout << "--------------------------------------------------------------------------------\n";
  HdlcEcFramePayload request[] = {
    {0x1, 0x2, 0x2, 0x1, 0x0},
    {0x2, 0x2, 0x2, 0x1, 0x0},
    {0x3, 0x2, 0x2, 0x1, 0x0},
    {0x4, 0x2, 0x2, 0x1, 0x0},
    {0x5, 0x2, 0x2, 0x1, 0x0},
    {0x6, 0x2, 0x2, 0x1, 0x0},
    {0x7, 0x2, 0x2, 0x1, 0x0},
    {0x8, 0x2, 0x2, 0x1, 0x0}
  };
  HdlcEcFramePayload reply;
  for (size_t i = 0; i < sizeof(request)/sizeof(HdlcEcFramePayload); i++) {
    sca_comm.execCommand(request[i], reply);
    if (reply.error)
      std::cout << sca_comm.getPayloadErrorMessage(reply.error) << std::endl;
  }
}

void
test4(ScaComm& sca_comm)
{
  std::cout << "Read GPIO data in" << std::endl;
  HdlcEcFramePayload request;
  HdlcEcFramePayload reply;

  std::cout << "\nHDLC : Register CRB -> 0xffffffff" << std::endl;
  request = {0x0, 0x0, 0x1, 0x2, 0xffffffff};
  sca_comm.execCommand(request, reply);

  std::cout << "\nHDLC : Read GPIO register DIRECTION" << std::endl;
  request = {0x0, 0x2, 0x1, 0x21, 0x0};
  sca_comm.execCommand(request, reply);

  std::cout << "\nHDLC : Set GPIO register DIRECTION -> 0x0a0b0c0d" << std::endl;
  request = {0x0, 0x2, 0x4, 0x20, 0x0a0b0c0d};
  sca_comm.execCommand(request, reply);

  std::cout << "\nHDLC : Read GPIO register DIRECTION" << std::endl;
  request = {0x0, 0x2, 0x1, 0x21, 0x0};
  sca_comm.execCommand(request, reply);

  std::cout << "\nHDLC : Set GPIO register DIRECTION -> 0x00001234" << std::endl;
  request = {0x0, 0x2, 0x4, 0x20, 0x00001234};
  sca_comm.execCommand(request, reply);

  std::cout << "\nHDLC : Read GPIO register DIRECTION" << std::endl;
  request = {0x0, 0x2, 0x1, 0x21, 0x0};
  sca_comm.execCommand(request, reply);

  std::cout << "\nHDLC : Set GPIO register DATAOUT -> 0x00000000" << std::endl;
  request = {0x0, 0x2, 0x4, 0x10, 0x00000000};
  sca_comm.execCommand(request, reply);

  std::cout << "\nHDLC : Set GPIO register DIRECTION -> 0x0000001f" << std::endl;
  request = {0x0, 0x2, 0x4, 0x20, 0x1f000000};
  sca_comm.execCommand(request, reply);

  std::cout << "\nHDLC : Read GPIO register DATAIN" << std::endl;
  request = {0x0, 0x2, 0x1, 0x01, 0x0};
  sca_comm.execCommand(request, reply);
}

void test5(ScaComm& sca_comm)
{
  Fec fec(sca_comm);
  std::string filename("/tmp/_delme_gbtx1-cfg.txt");
  std::cout << "Dumping GBTx1 configuration to " << filename << std::endl;

  std::ofstream os(filename, std::ios::out);
  if (!os.is_open())
    throw std::runtime_error("Unable to open file " + filename);

  fec.gbtxDumpConfiguration(1, os);
}

void fec_init(ScaComm& sca_comm)
{
  Fec fec(sca_comm);
  fec.init(0,1,0,1,1,0);

  ScaGpio& gpio(fec.getScaGpio());
  std::cout << gpio << std::endl;

  ScaAdc& adc(fec.getScaAdc());
  std::cout << adc << std::endl;
}


void fec_test(ScaComm& sca_comm) {
  Fec fec(sca_comm);
  std::cout << fec << std::endl;

//  ScaI2c& gbtx1(fec.getScaI2cGbtx(1));
//  for (int i = 24; i < 40; i++)
//     std::cout << std::hex << static_cast<uint32_t>(gbtx1.readByte(14, i)) << std::endl;

  for (uint8_t i = 0; i < 5; i++) {
    try {
      ScaI2c& sampa(fec.getScaI2cSampa(i));
      std::cout << "Sampa" << static_cast<uint32_t>(i) << " read I2C "
                << std::hex << static_cast<uint32_t>(sampa.readByte(0x7)) << std::dec << "\n";
    }
    catch(ScaException& e) {
      std::cout << e.what() << std::endl;
    }
  }

  ScaI2c& i2c1(fec.getScaI2cGbtx(1));
  std::cout << "GBTx1 read I2C " << std::hex << static_cast<uint32_t>(i2c1.readByte(14, 24)) << std::endl;
}



//------------------------------------------------------------------------------
int main(int argc, char** argv)
{
  bpo::variables_map vm;
  bpo::options_description opt_general(
    "Usage:  \n" + std::string(argv[0]) + " <cmds/options>\n"
    "  Developer tool for low-level and feature testing - You know what you do\n"
    "Commands / Options");
  bpo::options_description opt_hidden("");
  bpo::options_description opt_all;
  bpo::positional_options_description opt_pos;

  try
  {
    opt_general.add_options()
      ("help,h", "Print this help message")
      ("verbose,v", "Be verbose")
      ("version", "Print version information")
      //
      ("mask,m", bpo::value<cl_uint_t>()->default_value(0x1),
         "Mask defining on which SCAs to execute all following operations")
      //
      ("show-hdlc-retries", "Enable messages related to HDLC frame Tx retry")
      ("dbg0", "Run DBG routine")
      ("dbg1", "Run DBG routine")
      ("test,t", bpo::value<std::vector<int>>()->multitoken(), "Run tests")
      ;

    opt_all.add(opt_general).add(opt_hidden);

    bpo::store(bpo::command_line_parser(argc, argv).options(opt_all)
                     .positional(opt_pos).run(), vm);

    if (vm.count("help") || argc == 1) {
      std::cout << opt_general << std::endl;
      exit(0);
    }

    bpo::notify(vm);
  }
  catch(bpo::error& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << opt_general << std::endl;
    exit(1);
  }
  catch(std::exception& e)
  {
    std::cerr << e.what() << ", application will now exit" << std::endl;
    exit(2);
  }

  if (vm.count("version"))
    std::cout << GitInfo();

  // Do stuff
  int device_number(0);
  int bar_number(1);
  std::unique_ptr<trorc::Device> trorc;
  std::unique_ptr<trorc::Bar> bar;

  try {
    trorc.reset(new trorc::Device(device_number));
    bar.reset(new trorc::Bar(*trorc, bar_number));
  } catch (int e) {
    std::cerr << "ERROR: Failed to initialize T-RORC: " << librorc::errMsg(e)
              << std::endl;
    exit(1);
  }

  try {
    // FEC, or SCA loop
    for (uint32_t fec_idx = 0; fec_idx < 32; fec_idx++) {

      // Specified SCAs
      if ((vm["mask"].as<cl_uint_t>()() >> fec_idx) & 0x1) {

        if (vm.count("verbose"))
          std::cout << "Communicating with SCA" << fec_idx << std::endl;
      
        GscTrorcHdlcSw sca_comm(*bar, fec_idx, true, (vm.count("show-hdlc-retries") ? true : false));
      
        // Hidden
        if (vm.count("dbg0")) { sca_comm.dbg0(); std::cout << "DBG0\n"; }
        if (vm.count("dbg1")) { sca_comm.dbg1(); std::cout << "DBG1\n"; }
      
        // Run tests
        if (vm.count("test")) {
          auto t(vm["test"].as<std::vector<int>>());
          for (size_t i = 0; i < t.size(); i++) {
             switch(t[i]) {
               case 0:  test0(static_cast<ScaComm&>(sca_comm)); break;
               case 1:  test1(static_cast<ScaComm&>(sca_comm)); break;
               case 2:  test2(static_cast<ScaComm&>(sca_comm)); break;
               case 3:  test3(static_cast<ScaComm&>(sca_comm)); break;
               case 4:  test4(static_cast<ScaComm&>(sca_comm)); break;
               case 5:  test5(static_cast<ScaComm&>(sca_comm)); break;
               case 1000: fec_init(static_cast<ScaComm&>(sca_comm)); break;
               default: break;
             }
          }
        }
      }   // Specified SCAs
    }   // SCA loop
  }
  catch (std::exception& e) {
    std::cerr << e.what() << ", exiting" << std::endl;
    exit(100);
  }

  return 0;
}
