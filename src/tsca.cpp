#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <vector>
#include <boost/program_options.hpp>

#include "trorc.hpp"
#include "gbt_link.hpp"
#include "gbt_sca_comm.hpp"
#include "fec.hpp"
#include "git_info.hpp"

using namespace gbt;
namespace bpo = boost::program_options;

int main(int argc, char** argv)
{
  bpo::variables_map vm;
  bpo::options_description opt_general(
    "Usage:\n  " + std::string(argv[0]) + " <cmds/options>\n"
    "  Tool will apply the operations on all SCAs defined by the SCA mask\n"
    "Commands / Options");
  bpo::options_description opt_hidden("");
  bpo::options_description opt_all;
  bpo::positional_options_description opt_pos;

  try
  {
    opt_general.add_options()
      ("help,h", "Print this help message")
      ("verbose,v", "Be verbose")
      ("version", "Print version information")
      //
      ("mask,m", bpo::value<cl_uint_t>()->default_value(0x1),
         "Mask defining on which FECs/SCAs to execute all following operations")
      ("status,s", "Print HDLC core status")
      ("svl-connect", "Send supervisory-level command CONNECT to SCA")
      ("svl-reset", "Send supervisory-level command RESET to SCA")
      ("svl-test", "Send supervisory-level command TEST to SCA")
      ("hdlc-clr", "Clear hdlc_sw core statistics")
      ("hdlc-rst", "Reset hdlc_sw core")
      ("hdlc-tx-bitswap", bpo::value<uint8_t>(), "Control tx_2b bitswap")
      ("hdlc-rx-bitswap", bpo::value<uint8_t>(), "Control rx_2b bitswap")
      ("hdlc-tx-mux", bpo::value<uint8_t>(), "Set TX mux (1 -> gbt_hdlc_sw)")
      ;

    opt_all.add(opt_general).add(opt_hidden);

    bpo::store(bpo::command_line_parser(argc, argv).options(opt_all)
                     .positional(opt_pos).run(), vm);

    if (vm.count("help") || argc == 1) {
      std::cout << opt_general << std::endl;
      exit(0);
    }

    bpo::notify(vm);
  }
  catch(bpo::error& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << opt_general << std::endl;
    exit(1);
  }
  catch(std::exception& e)
  {
    std::cerr << e.what() << ", application will now exit" << std::endl;
    exit(2);
  }

  if (vm.count("version"))
    std::cout << GitInfo();

  // Do stuff
  int device_number(0);
  int bar_number(1);
  std::unique_ptr<trorc::Device> trorc;
  std::unique_ptr<trorc::Bar> bar;

  try {
    trorc.reset(new trorc::Device(device_number));
    bar.reset(new trorc::Bar(*trorc, bar_number));
  } catch (int e) {
    std::cerr << "ERROR: Failed to initialize T-RORC: " << librorc::errMsg(e)
              << std::endl;
    exit(1);
  }

  try {
    // FEC, or SCA loop
    for (uint32_t fec_idx = 0; fec_idx < 32; fec_idx++) {

      // Specified SCAs
      if ((vm["mask"].as<cl_uint_t>()() >> fec_idx) & 0x1) {

        if (vm.count("verbose"))
          std::cout << "Communicating with SCA" << fec_idx << std::endl;
      
        GscTrorcHdlcSw sca_comm(*bar, fec_idx, true, false);
      
        if (vm.count("svl-reset")) { 
          sca_comm.sendSvlReset();
          std::cout << "SVL-RESET sent by HDLC core" << fec_idx << "\n";
        }

        if (vm.count("svl-connect")) {
          sca_comm.sendSvlConnect();
          std::cout << "SVL-CONNECT sent by HDLC core" << fec_idx << "\n";
        }

        if (vm.count("svl-test")) {
          sca_comm.sendSvlTest();
          std::cout << "SVL-TEST sent by HDLC core" << fec_idx << "\n";
        }

        if (vm.count("hdlc-clr")) {
          sca_comm.clr();
          std::cout << "HDLC core " << fec_idx << " statistics cleared\n";
        }
        if (vm.count("hdlc-rst")) {
          sca_comm.rst();
          std::cout << "Core reset\n";
        }
        if (vm.count("hdlc-tx-mux")) {
          std::cout << "Tx mux set to "
                    << static_cast<uint32_t>(sca_comm.tx_mux_sel(vm["hdlc-tx-mux"].as<uint8_t>())) << "\n";
        }
        if (vm.count("hdlc-tx-bitswap")) {
          std::cout << "Tx bitswap set to "
                    << static_cast<uint32_t>(sca_comm.txBitswap(vm["hdlc-tx-bitswap"].as<uint8_t>())) << "\n";
        }
        if (vm.count("hdlc-rx-bitswap")) {
          std::cout << "Rx bitswap set to "
                    << static_cast<uint32_t>(sca_comm.rxBitswap(vm["hdlc-rx-bitswap"].as<uint8_t>())) << "\n";
        }
      
        // Print HDLC status
        if (vm.count("status"))
          std::cout << sca_comm << "\n";

      }   // Specified SCAs
    }   // SCA loop
  }
  catch (std::exception& e) {
    std::cerr << e.what() << ", exiting" << std::endl;
    exit(100);
  }

  return 0;
}
