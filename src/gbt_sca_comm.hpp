#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdint>
#include <chrono>

#include "trorc_registers.h"
#include "trorc.hpp"
#include "hdlc.hpp"


#define SUPPORT_HDLC_SW
//#define SUPPORT_HDLC_LIGHT

namespace gbt {

/** Interface class for GBT SCA communication
 *  @see GscTrorcHdlcLight
 */
class ScaComm
{
  private:
    static const std::string hdlcPayloadErrors_[]; /**< HDLC payload reply frame error codes, SCA manual v8.0 p14 */

  protected:
    const uint8_t transceive_max_retries_{5};
    const std::chrono::milliseconds transceive_timeout_{10};

    bool    request_trid_auto_; /**< Option for automatic transaction ID handling */
    uint8_t request_trid_;      /**< Next valid transaction ID */

    bool print_hdlc_dbg_;       /**< Print HDLC debug messages */

    /** Adjust request trid\n
     *  Valid trids are in range 0x1 .. 0xfe
     *  @see GBT SCA manual v8.0 p13f
     */
    void adjustRequestTrid(HdlcEcFramePayload& r)
      {
        r.trid = (request_trid_auto_ ? request_trid_ : r.trid);
        request_trid_ = (request_trid_ % 0xfe) + 1;
      }

  public:
    /** GBT SCA communication via HDLC frames
     *  @param request_trid_auto Enable automatic handling of transaction ID in HDLC frame payload
     *                           (User-specified transaction ID will be overwritten)
     */
    ScaComm(bool request_trid_auto = false, bool print_hdlc_dbg = true) :
        request_trid_auto_(request_trid_auto),
        request_trid_(1),
        print_hdlc_dbg_(print_hdlc_dbg)
      {}

    /** Error and return codes
     */
    enum return_codes_t : uint32_t {
      success = 0x0,
      error = 0xffffffff
    };

    /** Print core status
     */
    friend std::ostream& operator<< (std::ostream& os, const ScaComm& r) {
      return r.printStatus(os);
    }

    /** Ensure compatibility between this software and the
     *  underlying hardware
     */
    virtual bool     checkCompatibility() const { return false; }
  
    /** Print status information
     */
    virtual std::ostream& printStatus(std::ostream& os) const
      {
        os << "-> " << __func__ << " not implemented\n";
        return os;
      }

    /** Send SCA request frame. Intercept user-specified transaction ID and replace with automatically
     *    generated ID, if class option enabled
     *  @param r Payload of the request frame
     */
    virtual void     sendRequest(HdlcEcFramePayload& r) = 0;

    /** Get payload content of SCA HDLC reply frame\n
     *    Especially with gbt_hdlc_sw, sometimes more than one frame is encountered in the
     *    buffer. The function will try to recover the payload from the first frame received.
     *  @param r Buffer for payload of the reply frame
     *  @return 0 for r.channel == 0, else r.error
     */
    /* TODO: implement
    virtual uint32_t getReply(HdlcEcFramePayload& r) const = 0;
    */

    /** Get full SCA HDLC reply frame
     *  @param r Buffer for the reply frame
     *  @return Number of bits in frame, if a valid frame was received
     */
      virtual uint32_t getFrame(HdlcEcFrame& r) const = 0;

    /** Translate HDLC frame error code to string
     *  @param error_code Error code of the HDLC payload reply frame
     *  @return String indicated all active error conditions
     */
    std::string      getPayloadErrorMessage(uint8_t error_code) const;

    /** Execute SCA command cycle (transmit request and get reply).
     *   i.e. send request and (blocking) wait for reply frame with
     *   corresponding transaction ID
     *  @param request Payload of the request frame
     *  @param reply   Payload of the reply frame
     *  @return 0 if reply with correct transaction ID was received, non-zero otherwise
     *  @see getFrame
     *  @see sendRequest
     */
    virtual uint32_t execCommand(HdlcEcFramePayload& request, HdlcEcFramePayload& reply) = 0;

    /** Send supervisory level command "CONNECT" to SCA (see SCA manual p.11)
     */
    virtual void     sendSvlConnect() const = 0;

    /** Send supervisory level command "TEST" to SCA (see SCA manual p.11)
     */
    virtual void     sendSvlTest() const = 0;

    /** Send supervisory level command "RESET" to SCA (see SCA manual p.11)
     */
    virtual void     sendSvlReset() const = 0;
};


#if defined SUPPORT_HDLC_SW
class GscTrorcHdlcSw : public ScaComm
{
  public:
    /** Create interface for gbt_hdlc_sw core on T-RORC for communication with the GBT-SCA
     *  @param bar               Bar the core registers are found in
     *  @param request_trid_auto Enable automatic transaction ID handling
     *  @see ScaComm
     */
    GscTrorcHdlcSw(const trorc::Bar& bar,
                   uint8_t sca_idx = 0x0,
                   bool request_trid_auto = false,
                   bool print_hdlc_dbg = true) :
        ScaComm(request_trid_auto, print_hdlc_dbg),
        bar_(bar),
        sca_idx_(sca_idx)
      {}

    void     sendRequest(HdlcEcFramePayload& r) override;
    uint32_t getFrame(HdlcEcFrame& r) const override;
    uint32_t execCommand(HdlcEcFramePayload& request, HdlcEcFramePayload& reply) override;

    void     sendSvlConnect() const override { sendSFrame(HdlcSwEc256::SFT_CONNECT); }
    void     sendSvlTest() const override { sendSFrame(HdlcSwEc256::SFT_TEST); }
    void     sendSvlReset() const override { sendSFrame(HdlcSwEc256::SFT_RESET); }

    std::ostream& printStatus(std::ostream& os) const override;

    /** Swap Tx output bits\n
     *  @param val Multiplexer value\n
     *         0: tx_2b[1..0] <= tx_2b_i[1..0]\n
     *         1: tx_2b[1..0] <= tx_2b_i[0..1]\n
     *  @return Current bit value
     */
    inline uint32_t txBitswap(uint8_t val) const
      { return bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_BITSWAP_SEL, val); }


    /** Control T-RORC Tx MUX setting (1 -> gbt_hdlc_sw, 0 -> gbt_hdlc_light)
     *  @warning This is T-RORC specific and will be removed again 
     *  @param val MUX setting
     *  @return Current MUX setting
     */
    inline uint32_t tx_mux_sel(uint8_t val) const
      { return bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_MUX_SEL_DBG, val); }

    /** Swap Rx input bits\n
     *  @param val Multiplexer value\n
     *         0:  rx_2b_i[1..0] <= rx_2b[1..0]\n
     *         1:  rx_2b_i[1..0] <= rx_2b[0..1]\n
     *  @return Current bit value
     */
    inline uint32_t rxBitswap(uint8_t val) const
      { return bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_BITSWAP_SEL, val); }

    /** Clear statistics, counters, ...
     */
    inline void clr() const
      {
        bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_CLR, 0x1);
        bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_CLR, 0x0);
        bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_CLR, 0x1);
        bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_CLR, 0x0);
      }

    /** Trigger reset of hdlc_sw core
     */
    inline void rst() const
      {
        bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_USR_RST, 0x1);
        bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_USR_RST, 0x1);
        bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_USR_RST, 0x0);
        bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_USR_RST, 0x0);
      }

    void dbg0();
    void dbg1();

  private:
    const trorc::Bar& bar_;
    const uint8_t sca_idx_;
    HdlcSwEc256 hdlc_; 

    /** Test txReady bit
     */
    inline bool txReady() const
      { return bar_.readBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_DONE) == 1; }

    /** Test rxReady bit
     */
    inline bool rxReady() const
      { return bar_.readBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_DONE) == 1; }

    /** Pulse Tx start bit
     */
    void txStart() const
      {
        bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_START, 0x1);
        bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_START, 0x0);
      }

    /** Pulse Rx acknowledge bit
     */
    void rxAck() const
      { 
        bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_ACK, 0x1);
        bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_ACK, 0x0);
      }

    /** Trigger transmission of supervisory-level frame
     */
    void
    sendSFrame(HdlcSwEc256::sFrameType s) const
      {
        hdlc_sw_ec_256_t fb;
        hdlc_.sFrame(fb, s);
        txShregLoad(fb);
        txStart();
      }

    /** Get Rx sequence counter
     *  @return Rx SW sequence count
     */
    inline uint8_t rxSwseqCnt() const
      {
        return static_cast<uint8_t>(bar_.readField(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL,
                             F_RX_SWSEQ_CNT_LSB, F_RX_SWSEQ_CNT_WDT));
      }

    /** Get Tx sequence counter
     *  @return Tx SW sequence count
     */
    inline uint8_t txSwseqCnt() const
      {
        return static_cast<uint8_t>(bar_.readField(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL,
                             F_TX_SWSEQ_CNT_LSB, F_TX_SWSEQ_CNT_WDT));
      }

    /** Load Tx shift register with frame buffer data
     */
    void
    txShregLoad(const hdlc_sw_ec_256_t& fb) const
      {
        for (uint32_t i = 0; i < 8; i++)
          bar_.write(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_DATA_0 + i,
                       static_cast<uint32_t>((fb >> i*32) & 0xffffffff));
      }

    /** Fetch rx shift register content to frame buffer
     */
    void
    rxShregFetch(hdlc_sw_ec_256_t& fb) const
      {
        fb = 0x0;
        for (uint32_t i = 0; i < 8; i++)
          fb |= (hdlc_sw_ec_256_t(bar_.read(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_DATA_0 + i))
                 << (i * 32));
      }

    /** Increment software-controlled Rx sequence counter
     *  @return New SW Rx sequence count
     */
    inline uint8_t rxSwseqCntInc() const
      {
        bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_SWSEQ_CNT_INC, 0x1);
        bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_SWSEQ_CNT_INC, 0x0);
        return rxSwseqCnt();
      }

    /** Increment software-controlled Tx sequence counter
     *  @return New SW Tx sequence count
     */
    inline uint8_t txSwseqCntInc() const
      {
        bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_SWSEQ_CNT_INC, 0x1);
        bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_SWSEQ_CNT_INC, 0x0);
        return txSwseqCnt();
      }

    /** Base addresses in the T-RORC\n
     *    Assumes one SCA per FEC on GBTx0\n
     *    With multiple GBTx connected to the T-RORC, GBTx0s must be connected as link 0, 2, 4, ...
     */
    const uint32_t rx_base_addr_ = (2 * sca_idx_ + 1) * TRORC_CHANNEL_OFFSET + (1 << TRORC_REGFILE_GTXRX_SEL);
    const uint32_t tx_base_addr_ = (2 * sca_idx_ + 1) * TRORC_CHANNEL_OFFSET + (2 << TRORC_REGFILE_GTXRX_SEL);

    // in TRORC_REG_GBTTX_SCA_SW_CTRL
      const uint8_t B_TX_USR_RST       =  0;  // B -> Bit, F -> Field
      const uint8_t B_TX_CLR           =  1;
      const uint8_t B_TX_START         =  2;
      const uint8_t B_TX_DONE          =  3;
      const uint8_t B_TX_SWSEQ_CNT_INC =  4;
      const uint8_t B_TX_BITSWAP_SEL   =  8;
      const uint8_t F_TX_SEQ_CNT_LSB   = 16;
      const uint8_t F_TX_SEQ_CNT_WDT   =  4;
      const uint8_t F_TX_SWSEQ_CNT_LSB = 20;
      const uint8_t F_TX_SWSEQ_CNT_WDT =  4;
      const uint8_t B_TX_MUX_SEL_DBG   = 31;

    // in TRORC_REG_GBTRX_SCA_SW_CTRL
      const uint8_t B_RX_USR_RST       =  0;  // B -> Bit, F -> Field
      const uint8_t B_RX_CLR           =  1;
      const uint8_t B_RX_ACK           =  2;
      const uint8_t B_RX_DONE          =  3;
      const uint8_t B_RX_SWSEQ_CNT_INC =  4;
      const uint8_t B_RX_BITSWAP_SEL   =  8;
      const uint8_t F_RX_SEQ_CNT_LSB   = 16;
      const uint8_t F_RX_SEQ_CNT_WDT   =  4;
      const uint8_t F_RX_SWSEQ_CNT_LSB = 20;
      const uint8_t F_RX_SWSEQ_CNT_WDT =  4;
};
#endif  // SUPPORT_HDLC_SW


#if defined SUPPORT_HDLC_LIGHT
/** Class implementing the communication with the GBT SCA on a T-RORC, which
 *  uses the hdlc_light SCA communication core
 */
class GscTrorcHdlcLight : public ScaComm
{
  public:
    /** Create interface for gbt_hdlc_light core on T-RORC for communication with the GBT-SCA
     *  @param bar               Bar the core registers are found in
     *  @param request_trid_auto Enable automatic transaction ID handling
     *  @see ScaComm
     */
    GscTrorcHdlcLight(const trorc::Bar& bar,
                      bool request_trid_auto = false,
                      bool print_hdlc_dbg = true) :
        ScaComm(request_trid_auto, print_hdlc_dbg),
        bar_(bar)
      {}

    void     sendRequest(HdlcEcFramePayload& r) override;
    uint32_t getFrame(HdlcEcFrame& r) const override;
    uint32_t execCommand(HdlcEcFramePayload& request, HdlcEcFramePayload& reply) override;

    void     sendSvlConnect() const override { sendSvlCommand(svl_connect); }
    void     sendSvlTest() const override { sendSvlCommand(svl_test); }
    void     sendSvlReset() const override { sendSvlCommand(svl_reset); };

    std::ostream& printStatus(std::ostream& os) const override;

  private:
    const Bar& bar_;

    /** Get CRC transmitted with the HDLC packet and CRC recalculated by the hardware
     *  @return CRC match
     */
    bool getReplyCrc(uint32_t& crcRecalc, uint32_t& crcReceived) const;

    inline bool txReady() const
      { return bar_.readBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_CTRL, B_TX_READY); }

    inline bool rxReady() const
      { return bar_.readBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_CTRL, B_RX_READY); }

    /** Codes for (supervisory level) commands, which control the 'tx_mode'
     */
    enum svl_cmd_codes_t {
      svl_packet = 0x0,
      svl_connect = 0x1,
      svl_reset = 0x2,
      svl_test = 0x3
    };

    void    sendSvlCommand(svl_cmd_codes_t c) const;

    /** Transfer payload struct content to write register
     */
    inline void
    assembleRegisters(const HdlcEcFramePayload& r, uint32_t& w2, uint32_t& w3) const {
        w2 = ((r.channel << 8)  |
              (r.trid    << 0)  |
              (r.length  << 16) |
              (r.command << 24));
        w3 = r.data;
      }

    /** Pack register read values into payload struct
     */
    inline void
    disassembleRegisters(HdlcEcFramePayload& r, uint32_t w2, uint32_t w3) const {
        r.channel = (w2 >> 0)  & 0xff;
        r.trid    = (w2 >> 8)  & 0xff;
        r.error   = (w2 >> 16) & 0xff;
        r.length  = (w2 >> 24) & 0xff;
        r.data    = w3;
      }

    // Base addresses
    const uint32_t rx_base_addr_ = TRORC_CHANNEL_OFFSET + (1 << TRORC_REGFILE_GTXRX_SEL);
    const uint32_t tx_base_addr_ = TRORC_CHANNEL_OFFSET + (2 << TRORC_REGFILE_GTXRX_SEL);

    // in TRORC_REG_GBTTX_SCA_CTRL
      const uint8_t B_TX_START         =  0;  // B -> Bit, F -> Field
      const uint8_t F_TX_MODE_LSB      =  1;
      const uint8_t F_TX_MODE_WDT      =  2;
      const uint8_t B_TX_READY         =  3;
      const uint8_t F_TX_HDLC_ADDR_LSB =  8;
      const uint8_t F_TX_HDLC_ADDR_WDT =  8;
      const uint8_t F_TX_FSM_LSB       = 20;
      const uint8_t F_TX_FSM_WDT       =  4;
      const uint8_t F_TX_SEQNR_LSB     = 24;
      const uint8_t F_TX_SEQNR_WDT     =  8;

    // in TRORC_REG_GBTRX_SCA_CTRL
      const uint8_t B_RX_START         =  0;
      const uint8_t B_RX_READY         =  2;
      const uint8_t B_RX_SVL_CMD       =  3;
      const uint8_t F_RX_FSM_LSB       =  4;
      const uint8_t F_RX_FSM_WDT       =  4;
      const uint8_t F_RX_HDLC_CTRL_LSB =  8;
      const uint8_t F_RX_HDLC_CTRL_WDT =  8;
      const uint8_t F_RX_HDLC_ADDR_LSB = 16;
      const uint8_t F_RX_HDLC_ADDR_WDT =  8;
      const uint8_t F_RX_SEQNR_LSB     = 24;
      const uint8_t F_RX_SEQNR_WDT     =  8;
    
    // in TRORC_REG_GBTRX_SCA_CRC
      const uint8_t F_RX_CRC_RECEIVED_LSB =  0;
      const uint8_t F_RX_CRC_RECEIVED_WDT = 16;
      const uint8_t F_RX_CRC_RECALC_LSB   = 16;
      const uint8_t F_RX_CRC_RECALC_WDT   = 16;
};
#endif // SUPPORT_HDLC_LIGHT

} // namepsace gbt
