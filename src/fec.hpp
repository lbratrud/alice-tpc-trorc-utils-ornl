#pragma once

#include <fstream>
#include <array>
#include <assert.h>

#include "gbt_sca_comm.hpp"
#include "gbt_sca.hpp"
#include "gbt_sca_i2c.hpp"
#include "sampa.hpp"

/** FEC class encapsulating
 *    GPIO, I2C, ...
 */
class Fec
{
  protected:
    static const uint8_t n_gbtx_{2};
    static const uint8_t n_sampa_{5};

    std::unique_ptr<gbt::ScaBasic> sca_basic_;
    std::unique_ptr<gbt::ScaGpio> sca_gpio_;
    std::unique_ptr<gbt::ScaAdc> sca_adc_;

    std::array<std::unique_ptr<gbt::ScaI2c>, n_gbtx_>  i2c_gbtx_;
    std::array<std::unique_ptr<gbt::ScaI2c>, n_sampa_> i2c_sampa_;

    std::array<std::unique_ptr<Sampa>, n_sampa_> sampa_;

    const uint32_t cfg_basic_crb_{0x9c}; /**< Configuration for GBT-SCA register CRB */
    const uint32_t cfg_basic_crc_{0x0f}; /**< Configuration for GBT-SCA register CRC */
    const uint32_t cfg_basic_crd_{0x10}; /**< Configuration for GBT-SCA register CRD */

    const uint32_t cfg_gpio_direction_{0xffc003ff}; /**< GPIO pin direction: All output, except for board Id*/

    const uint32_t cfg_adc_gaincalib_{0x1000};  /**< ADC gain calibration (gain = 1.0) */
    // const uint32_t cfg_adc_gaincalib_{0x0d99};  /**< ADC gain calibration (gain = .85) */

    const uint8_t cfg_i2c_addr_gbtx_[n_gbtx_]{0xf, 0xe};                  /**< I2C addresses of GBTx0 and GBTx1 */
    const uint8_t cfg_i2c_addr_sampa_[n_sampa_]{0x0, 0x1, 0x2, 0x3, 0x4}; /**< I2C addresses of the SAMPAs */

    const uint8_t REG_SAMPA_SOCFG  {0x12};
    const uint8_t REG_SAMPA_ADCTRIM {0x11};
    const uint8_t REG_SAMPA_SODRVST{0x13};
    const uint8_t REG_SAMPA_CLKCONF{0x22};

    uint8_t verbosity_;

  private:
    /** Get ADC reading and convert it to user-ready value
     *  @param adc_pin ADC input pin to sample
     *  @param scaling Scaling parameter
     *  @param raw Show ADC raw value instead of converted value. If true, scaling param is ignored
     *  @return Raw ADC value if raw = true, else (ADC read value) / 4096 * scaling
     */
    inline float adcConversion(uint8_t adc_pin, float scaling, bool raw = false) const
      {
        return static_cast<float>(sca_adc_->sample(adc_pin)) * (raw ? 1. : (1. / 4096. * scaling));
      }

  public:
    Fec(gbt::ScaComm& sca_comm, uint8_t verbosity = 0) :
        sca_basic_(new gbt::ScaBasic(sca_comm)),
        sca_gpio_(new gbt::ScaGpio(sca_comm)),
        sca_adc_(new gbt::ScaAdc(sca_comm)),
        verbosity_(verbosity)
      {
        for (uint8_t i = 0; i < n_gbtx_; i++)
          i2c_gbtx_[i].reset(new gbt::ScaI2cGbtx(sca_comm, i, cfg_i2c_addr_gbtx_[i]));
        for (uint8_t i = 0; i < n_sampa_; i++) {
          i2c_sampa_[i].reset(new gbt::ScaI2cSampa(sca_comm, i + 4, cfg_i2c_addr_sampa_[i]));
          sampa_[i].reset(new Sampa(static_cast<gbt::ScaI2cSampa&>(*i2c_sampa_[i])));
        }
      }

    /** Initialize FEC\n
     *    - Enable SCA channels via CRB, CRC, CRD
     *    - Initialize GPIO
     *      - Set DATAOUT to 0
     *      - Set DIRECTION to cfg_direction_
     *    - Initialize ADC
     *      - Set gain calibration to cfg_adc_gaincalib_
     *      - Select default input
     *    - Initialize I2C interfaces
     *      - Set port frequency
     */
    void init(int CG0, int CG1,int CTS,int POL,int init,int V2) const;

    /** Set class verbosity\n
     *  Will write some information to std::cout if verbosity_ > 0
     */
    uint8_t setVerbosity(uint8_t v)
      {
        verbosity_ = v;
        return verbosity_;
      }

    /** Get pointer to GBT SCA GPIO instance
     *  @note Object lifetime managed by FEC class
     *  @return GPIO instance pointer
     */
    gbt::ScaGpio& getScaGpio()   const { return *(sca_gpio_.get()); }

    /** Get pointer to GBT SCA basic instance
     *  @note Object lifetime managed by FEC class
     *  @return SCA basic instance pointer
     */
    gbt::ScaBasic& getScaBasic() const { return *(sca_basic_.get()); }

    /** Get pointer to the GBT SCA ADC instance
     *  @note Object lifetime managed by FEC class
     *  @return ADC instance pointer
     */
    gbt::ScaAdc& getScaAdc()     const { return *(sca_adc_.get()); }

    /** Get pointer to GBT SCA I2C for GBTx instance
     *  @note Object lifetime managed by FEC class
     *  @param gbtx_idx GBTx index, valid are [0, 1]
     *  @return SCA I2C GBTx instance pointer
     */
    gbt::ScaI2c& getScaI2cGbtx(uint8_t gbtx_idx = 0) const
      {
        assert((gbtx_idx | 0x1) == 0x1);
        return *(i2c_gbtx_[gbtx_idx & 0x1].get());
      }

    /** Get pointer to GBT SCA I2C for SAMPA instance
     *  @note Object lifetime managed by FEC class
     *  @param sampa_idx SAMPA index, valid are [4..0]
     *  @return SCA I2C SAMPA instance pointer
     */
    gbt::ScaI2c& getScaI2cSampa(uint8_t sampa_idx) const
      {
        assert((sampa_idx >= 0) && (sampa_idx <= 4));
        return *(i2c_sampa_[sampa_idx].get());
      }
    
    /** Control SAMPA power on SCA GPIO[4..0]
     *  @param value Mask [4..0] representing the value to
               set on the five SAMPA power GPIO pins
     *  @param mask Mask [4..0] controlling which GPIO pins actually
               get altered [4..0]
     *  @return Current power status (0x1f if all SAMPAs powered)
     */
    uint32_t sampaPwrCtrl(uint32_t value, uint32_t mask = 0x1f) const;

    /** Switch on all SAMPAs
     *  @return Current power status
     *  @see sampaPwrCtrl
     */
    inline uint32_t sampaPwrOn(uint32_t mask = 0x1f) const { return sampaPwrCtrl(0x1f, mask); }

    /** Switch off all SAMPAs
     *  @return Current power status
     *  @see sampaPwrCtrl
     */
    inline uint32_t sampaPwrOff(uint32_t mask = 0x1f) const { return sampaPwrCtrl(0x0, mask); }

    /** Get SAMPA power status
     *  @return Vreg power enable, i.e. value of GPIO pins [4..0]
     */
    inline uint32_t sampaPwrStatus() const { return sca_gpio_->getDataIn() & 0x1f; }

    /** Enable SAMPA e-links
     *  @param sampa_mask Mask of SAMPAs to apply this setting to
     *  @param n Number of enabled e-links
     *  @return True if setting applied correctly
     */
    bool sampaEnableElinks(uint8_t n = 11, uint32_t sampa_mask = 0x1f) const;
    bool sampaWriteReg(uint8_t n = 11, uint32_t sampa_mask = 0x1f, uint8_t addr=0x12) const;
    bool sampaAdcTrim(uint8_t n = 4, uint32_t sampa_mask = 0x1f) const;
    bool sampaReadClkConf(uint8_t n = 11, uint32_t sampa_mask = 0x1f) const;
    bool sampaReadRegs(uint8_t n = 11, uint32_t sampa_mask = 0x1f) const;

    /** Scans for SAMPAs on I2C lines and allows to print information about found SAMPAs
     *  @param os Pointer to output stream or nullptr if printing is not desired 
     *  @return Mask of active SAMPAs found
     */
    uint32_t sampaI2cScan(std::ostream* const os = nullptr) const;

    /** Read FEC board ID
     *  @return Board ID, i.e. value of GPIO pins [21..10]
     */
    inline uint16_t adcBoardId() const { return (sca_gpio_->getDataIn() >> 10) & 0xfff; }

    /** Read GBT SCA internal temperature sensor
     *  @param raw Show ADC raw value instead of converted value
     *  @return Raw or converted SCA temperature
     *  @warning: TODO: Adjust scaling parameter for to convert to real temperature
     */
    inline float adcScaTemperature(bool raw = false) const { return adcConversion(31, 1., raw); }

    /** Get raw VTRx ADC voltage reading
     *  @param raw Show ADC raw value instead of converted value
     *  @return Raw or converted voltage
     *  @warning TODO: Adjust scaling parameter, is invalid on FECv0
     */
    inline float adcVoltageVtrx(bool raw = false) const { return adcConversion(0, 1., raw); }

    /** Get 1.5V voltage reading (via SCA ADC)
     *  @param raw Show ADC raw value instead of converted value
     *  @return Raw or converted voltage
     */
    inline float adcVoltage1v5(bool raw = false) const { return adcConversion(6, 2., raw); }

    /** Get 2.5V voltage reading (via SCA ADC)
     *  @param raw Show ADC raw value instead of converted value
     *  @return Raw or converted voltage
     */
    inline float adcVoltage2v5(bool raw = false) const { return adcConversion(7, 3., raw); }

    /** Configure GBTx via SCA I2C\n
     *    Configuration is read from file
     *  @param gbtx_idx GBTx index
     *  @param cfg_file Path to GBTx configuration file (in standard format with one register value per line)
     *  @throws std::runtime_error
     */
    void gbtxConfigure(uint8_t gbt_idx, const std::string& cfg_file) const;

    /** Dump GBTx configuration to stream
     *  @param gbt_idx GBTx index
     *  @param os Output stream
     */
    void gbtxDumpConfiguration(uint8_t gbt_idx, std::ostream& os = std::cout) const;

    /** Reset GBTx via SCA I2C\n
     *    Pulses bits 5..3 of GTBx register 50 (wdogCtr0, i2cResetRx[C, B, A])
     *    to reset RX control state machine and RX data path
     *  @param GBT index [0, 1]
     */
    void gbtxReset(uint8_t gbt_idx) const;

    /** Scans addresses for reply of GBTx via I2C and allows to print information about the
     *    scanning process
     *  @param gbt_idx GBTx I2C interface to probe [0,1]
     *  @param os Pointer to output stream or nullptr if printing is not desired
     *  @return Address of the GBTx found, -1 in case no GBTx is found on given I2C interface
     */
    int32_t gbtxI2cScan(uint8_t gbt_idx = 0, std::ostream* const os = nullptr) const;

    /** Print FEC board ID string
     *  @param Stream to output to
     */
    void printIdString(std::ostream& os) const
    {
      uint32_t board_id = static_cast<uint32_t>(adcBoardId());
      os << "FEC #" << std::dec << board_id << " [0x" << std::hex << board_id << std::dec << "]" << "\n";
    }
    void printRTD(std::ostream& os,const std::string& rtd, const std::string&desc,float vdef, float vlars) const
    {
        os << "parameter " << rtd << " " << std::setprecision(3) << 259.7*vdef / vlars - 259.88 << "\n";
        os << rtd << "=" << std::setprecision(3) << 259.7*vdef / vlars - 259.88 << "C " << desc << "=";
	os << std::setprecision(3) << vdef/4096 << "V\n";
    }
    void printCurMeasure(std::ostream& os,const std::string& rtd, const std::string&desc,float v1, float v2,float scale) const
    {
        os << "parameter " << rtd << " " << std::setprecision(4) << 244e-5/scale*(v1-v2) << "\n";
        os << rtd << "=" << std::setprecision(4) << 244e-5/scale*(v1-v2) << "A " << desc << "=[";
        os << std::setprecision(4) << v1/4096 << "V,";
        os << std::setprecision(4) << v2/4096 << "V]\n";
    }
    float getAdcAvgSamples(std::ostream& os,const Fec& f,uint8_t adc_index,uint8_t num_samples,bool enableCs) const
    {
       float sum=0;
       for(unsigned int i=0;i<20*num_samples;i++)
       {
             sum += static_cast<uint32_t>(f.getScaAdc().sample(adc_index, enableCs));
       }
       return 0.05*sum/num_samples;
    }

    /** Print FEC status summary
     */
    void getAdcSamples(std::ostream& os, const Fec& f,uint32_t num_samples)
      {
        bool enableCs(false);
	uint16_t vadc;

        f.printIdString(os);
        os << "  GainCalib 0x" << std::hex << f.sca_adc_->getGainCalib() << "\n";
        os << "  Gain 0x" << std::hex << f.sca_adc_->getGain() << "\n";
        os << "  ScaID 0x" << std::hex << f.sca_adc_->getID() << "\n";
	os << "\n";
	os << "ADC Channel Definitions:\n";
	os << "ADC inputs are pulled low if not in the definitions list\n";
	for(int i=0;i<32;i++)
	{
		enableCs = false;
		vadc = 0x1000;
		if (i==30)
		{
			os << "ADC[30] VTRx-Divider";
			vadc = (int)(4096*0.556);
		}
		if ((i>=25) and (i<30))
		{
			 os << "ADC["<< std::dec << i << "] TEMP Resistor";
			 vadc=(int)(4096*0.005);
			 enableCs = true;
		}
		if (i==23)
		{
			os << "ADC[23] V2.5 Divider";
			vadc = (int)(4096*2.5/3);
		}
		if (i==24)
		{
			os << "ADC[24] V1.5 Divider";
			vadc = (int)(4096*1.5/2);
		}
		if (i==31)
		{
			os << "ADC[31] SCA Temp";
			vadc = 0xc77; 
		}
		if (vadc != 0x1000)
		{
			os << " Expecting 0x" << std::hex << vadc << "(";
			os << std::setprecision(3) << 1.0/4096*vadc;
			os << ")\n";
        		os << "ADC["<< std::dec << i << "] SAMPLES:\n";
        		os << "SAMPLE# AdcValue(decimal) VoltValue\n";
			for(int j=0;j<num_samples;j++)
			{ 
				vadc = static_cast<uint32_t>(f.getScaAdc().sample(i, enableCs));
				os << std::dec << j << " ";
				os << " " << std::dec << vadc << " ";
				if (vadc & 0x1000)
				{
					os << "OVFL";
				}
				else
				{
					os << std::setprecision(4) << 1.0/4096*vadc;
				}
				os << "\n";
			}
			os << "\n";
		}
	}
      }
    /** Print FEC status summary
     */
    friend std::ostream& operator<<(std::ostream& os, const Fec& f)
      {
        bool enableCs(false);
	uint16_t vadc;
	float vlars,vdef;

        f.printIdString(os);
        os << "  GainCalib 0x" << std::hex << f.sca_adc_->getGainCalib() << "\n";
        os << "  Gain 0x" << std::hex << f.sca_adc_->getGain() << "\n";
        os << "  ScaID 0x" << std::hex << f.sca_adc_->getID() << "\n";
        os << "parameter ScaID 0x" << std::hex << f.sca_adc_->getID() << "\n";
/*
        for(int i=1;i<256;i+=0x10)
	{
           os << "ADC Register[0x" << std::hex << i << "]=";
	   for(int j=0;j<4;j++)
	   { 
		os  << " 0x" << std::hex << f.sca_adc_->getReg(i);
	   };
	   os << "\n";
	};
        for(int i=1;i<256;i+=0x10)
	{
           os << "GPIO Register[0x" << std::hex << i << "]=";
	   for(int j=0;j<4;j++)
	   { 
		os  << " 0x" << std::hex << f.sca_gpio_->getReg(i);
	   };
	   os << "\n";
	};
*/
	/*
		# RTD temperature conversion
		# 259.7*ADC[x]/ADC[18]-259.88 
		RTD1 (upper right) 26
		RTD2 (lower left) 25
		RTD3 (lower right) 28
		RTD4 (upper left) 27
		RTD5 (middle)  29
		SCA Temp 31
         */
        vlars = f.getAdcAvgSamples(os,f,18,5,true);
        os << "parameter SCAcurrent=" << std::setprecision(3) << vlars/4.096 << "\n";
        os << "SCA current=" << std::setprecision(3) << vlars/4.096 << "uA ADC[18]=" << std::setprecision(4) << vlars/4096 << "V\n";
        vdef = f.getAdcAvgSamples(os,f,26,5,true);
        f.printRTD(os,"RTD1","(upper right) ADC[26]",vdef,vlars);

        vdef = f.getAdcAvgSamples(os,f,25,5,true);
        f.printRTD(os,"RTD2","(lower left) ADC[25]",vdef,vlars);
	
        vdef = f.getAdcAvgSamples(os,f,28,5,true);
        f.printRTD(os,"RTD3","(lower right) ADC[28]",vdef,vlars);
	
        vdef = f.getAdcAvgSamples(os,f,27,5,true);
        f.printRTD(os,"RTD4","(upper left) ADC[27]",vdef,vlars);
	
        vdef = f.getAdcAvgSamples(os,f,29,5,true);
        f.printRTD(os,"RTD5","(middle) ADC[29]",vdef,vlars);
	
        vdef = f.getAdcAvgSamples(os,f,19,5,false);
        vlars = f.getAdcAvgSamples(os,f,20,5,false);
        f.printCurMeasure(os,"Current[2.5]","ADC[19,20]",vdef,vlars,0.3333);

        vdef = f.getAdcAvgSamples(os,f,21,5,false);
        vlars = f.getAdcAvgSamples(os,f,22,5,false);
        f.printCurMeasure(os,"Current[3.25]","ADC[21,22]",vdef,vlars,0.1667);

        vdef = f.getAdcAvgSamples(os,f,30,5,false);
        os << "VTRX diode monitor ADC[30]=" << std::setprecision(4) << vdef/4096 << "V\n";

        vdef = f.getAdcAvgSamples(os,f,24,5,false);
        os << "VoltageDivider[1.5V] ADC[24]=" << std::setprecision(4) << vdef/4096 << "V (expected 0.75V)\n";
        vdef = f.getAdcAvgSamples(os,f,23,5,false);
        os << "VoltageDivider[2.5V] ADC[23]=" << std::setprecision(4) << vdef/4096 << "V (expected 0.83V)\n";

        vdef = f.getAdcAvgSamples(os,f,31,5,false)/4.096;
        os << "SCA Temp=" << std::setprecision(3) << (vdef-765)*100/(580-765)-25 << "C ADC[31]=" << std::setprecision(3) <<  vdef << "mV\n";
	/*
		Lars 1K Resistor (SCA measuring current) 18

		# 244e-6*(ADC[19]-ADC[20])/(0.3333*0.1)
		# 244e-6*(ADC[21]-ADC[22])/(0.1667*0.1)
		2.25V current monitor 19
		2.25V current monitor 20
		3.25V current monitor 21
		3.25V current monitor 22
		2.5V voltage divider 23
		1.5V voltage divider 24
		VTRX diode monitor 30
	*/
	//os << " AvgSamples " << 1.0*vadc/4096 << "\n";
	/*
	for(int k=0;k<2;k++)
	{
		os << "\n";
		if (k==0)
		{
			os << "Current Sources Disabled\n";
			enableCs = false;
		}
		else
		{
			os << "Current Sources Enabled\n";
			enableCs = true;
		}
        	for(int i=0;i<32;i++)
		{
		   if (i >= 25)
		   {
                        enableCs = true;
		   }
                   else
		   {
			enableCs = false;
		   }
        	   os << "ADC ["<< std::dec << i << "]= ";
		   for(int j=0;j<5;j++)
		   { 
			vadc = static_cast<uint32_t>(f.getScaAdc().sample(i, enableCs));
			os << " 0x" << std::hex << vadc << "(";
			if (vadc & 0x1000)
			{
				os << "OVFL";
			}
			else
			{
				os << std::setprecision(3) << 1.0/4096*vadc;
			}
			os << ")";
		   };
        	   os << "\n";
        	};
	};
	*/
	os << "\n";
        os << "  GBTx0 I2C   addr " << f.getScaI2cGbtx(0).getSlaveAddress() << " (sw_cfg)\n"
           << "  GBTx1 I2C   addr " << f.getScaI2cGbtx(1).getSlaveAddress() << " (sw_cfg)\n"
/*
           << "  sampaPwr    0x" << std::hex << static_cast<uint32_t>(f.sampaPwrStatus()) << std::dec << "\n"
           << " -- ADC measurements not yet understood --\n"
           << "  v1_5        " << f.adcVoltage1v5() << " V "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage1v5(true)) << "] " << std::dec
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage1v5(true)) << "] " << std::dec
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage1v5(true)) << "] " << std::dec
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage1v5(true)) << "] " << std::dec
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage1v5(true)) << "] " << std::dec
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage1v5(true)) << "] " << std::dec
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage1v5(true)) << "] " << std::dec
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage1v5(true)) << "] " << std::dec
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage1v5(true)) << "] " << std::dec
                               << std::hex << "[raw. 0x" << static_cast<uint32_t>(f.getScaAdc().sample(6, enableCs)) << "]\n" << std::dec
           << "  v2_5        " << f.adcVoltage2v5() << " V "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage2v5(true)) << "] "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage2v5(true)) << "] "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage2v5(true)) << "] "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage2v5(true)) << "] "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage2v5(true)) << "] "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage2v5(true)) << "] "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage2v5(true)) << "] "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage2v5(true)) << "] "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltage2v5(true)) << "] "
                               << std::hex << "[raw. 0x" << static_cast<uint32_t>(f.getScaAdc().sample(7, enableCs)) << "]\n" << std::dec
           << "  vVTRx       " << f.adcVoltageVtrx() << " V "
                               << std::hex << "[raw 0x" << static_cast<uint32_t>(f.adcVoltageVtrx(true)) << "] "
                               << std::hex << "[raw. 0x" << static_cast<uint32_t>(f.getScaAdc().sample(0, enableCs)) << "]\n" << std::dec
           << "  scaTemp     " << "[raw " << f.adcScaTemperature(true) << "] "
           << "[raw w/cs " << static_cast<uint32_t>(f.getScaAdc().sample(31, true)) << "]\n"
*/
           << "";
        return os; 
      }
};
