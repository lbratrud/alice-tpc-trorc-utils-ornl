#pragma once

#include "gbt_sca_i2c.hpp"

class Sampa
{
  private:
    gbt::ScaI2cSampa& i2c_;

  public:
    Sampa(gbt::ScaI2cSampa& i2c) : i2c_(i2c) {}

    /** Write to SAMPA via I2C
     *  @param addr SAMPA register address
     *  @param val  Register write value
     *  @param mask 
     */
    uint8_t writeGlobalReg(uint8_t addr, uint8_t val) const { return 0; }

};
