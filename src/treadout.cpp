#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <thread>
#include <chrono>
#include <mutex>
#include <chrono>
#include <atomic>
#include <bitset>
#include <fstream>
#include <boost/program_options.hpp>
#include <boost/thread.hpp>           // Use boost::thread, C++11's threads doesn't support thread groups
#include <boost/filesystem.hpp>
#include <csignal>

#include "trorc.hpp"
#include "git_info.hpp"
#include "gbt_link.hpp"

namespace bpo = boost::program_options;
namespace bfs = boost::filesystem;

// #define SIMULATION_NORORC
static const uint32_t MAX_FECS(6);
static const uint32_t MAX_CHANNELS(2*MAX_FECS);
using fec_mask_t = std::bitset<MAX_FECS>;
using channel_mask_t = std::bitset<MAX_CHANNELS>;



//------------------------------------------------------------------------------
/** Trivial logger class
 *   for logging to stream from multiple threads
 */
class Log
{
  public:
    /** Log levels
     */
    enum level_t : int32_t {
      lsys = 0,
      linfo,
      lwarn,
      lerror,
      lfatal
    };

    /** Logging specialization for different log levels 
     *
     *  Resulting call order via log base templates is, e.g.
     *    lwarn -> log(lwarn, ...) -> log(...) -> ... -> log()
     */
    template<typename ...R>
    static void sys(R && ...r) { log(lsys, std::forward<R>(r)...); };
    template<typename ...R>
    static void info(R && ...r) { log(linfo, std::forward<R>(r)...); };
    template<typename ...R>
    static void warn(R && ...r) { log(lwarn, std::forward<R>(r)...); };
    template<typename ...R>
    static void error(R && ...r) { log(lerror, std::forward<R>(r)...); };
    template<typename ...R>
    static void fatal(R && ...r) { log(lfatal, std::forward<R>(r)...); };

    /** Change log level
     *  @param l New log level
     */
    static void setLevel(level_t l) { targetLogLevel_ = l; }

    static std::string levelName(level_t l) { return levelNames_[l]; }

  private:
    static std::mutex mtx_;
    static level_t targetLogLevel_;

    /** Logging base template
     */
    static void log() { std::cout << "\n"; }

    template<typename ...R>
    static void log(level_t lvl, R && ...r)
      {
        if (lvl >= targetLogLevel_) {
          std::lock_guard<std::mutex> g(mtx_);
          std::cout << "[" << lvl << "] :";
          Log::log(std::forward<R>(r)...);
        }
      }

    template<typename F, typename ...R>
    static void log(F && f, R && ...r)
      {
        std::cout << " " << std::forward<F>(f);
        Log::log(std::forward<R>(r)...);
      }

  protected:
    static const std::string levelColors_[];
    static const std::string levelNames_[];

    friend std::ostream& operator<< (std::ostream& s, const Log::level_t l)
    {
      s << Log::levelColors_[l] << Log::levelNames_[l] << "\033[;39m";
      return s;
    }
};

const std::string Log::levelNames_[] = {
  "SYS",
  "INFO",
  "WARN",
  "ERROR",
  "FATAL"
};

const std::string Log::levelColors_[] = {
  "\033[0;36m  ",
  "\033[0;32m ",
  "\033[0;33m ",
  "\033[0;31m",
  "\033[1;31m"
};

std::mutex Log::mtx_;
Log::level_t Log::targetLogLevel_(Log::lsys);



//------------------------------------------------------------------------------
/** File writer
 *
 *  Encapsulates the filename generation and handling/placing of the DMA
 *  channel output files
 */
class FileWriter
{
  private:
    std::ofstream ofs_;

  public:
    /** File writer options struct
     */
    struct Options {
      uint32_t device_id;
      channel_mask_t active_channel_mask;
      std::string base_dir;
      uint32_t run_number;
      bool omit_run_dir;
      bool overwrite;
    };

    /** Construct file writer object
     *  @param o       File writer options struct
     *  @param channel T-RORC DMA channel ID
     */
    FileWriter(const Options& o, uint32_t channel) :
        ofs_((outputDirPath(o) /= outputFilename(o, channel)).native(), 
          std::ofstream::out | std::ofstream::binary | std::ofstream::trunc)
      {
        if (!ofs_.is_open())
          throw std::runtime_error("Unable to open output stream " + 
            (outputDirPath(o) /= outputFilename(o, channel)).native() +
            " for writing");
      }

    /** Destruct
     */
    ~FileWriter()
      { ofs_.close(); }

    /** Write data to output stream
     *  @param data Pointer to the data to write
     *  @param size Data size in bytes
     */
    void write(const char* data, ssize_t size) { ofs_.write(data, size); }

    /** Flush output stream
     */
    void flush() { ofs_.flush(); }

    /** Prepend software header to events
     */
    void prependSwHeader(uint32_t channel, uint32_t words, uint64_t event_count)
      {
        uint32_t h[8];
        h[0] = (0x0 << 24) | ((channel & 0xf) << 20) | (0xf << 16);
        h[1] = words + 8;
        h[2] = 0xdeadbeef;
        h[3] = 0xdeafaffe;
        h[4] = (event_count >> 32) & 0xffffffff;
        h[5] = event_count & 0xffffffff;
        h[6] = 0x3fec2fec;
        h[7] = 0x1fec0fec;

        write(reinterpret_cast<const char*>(&h), sizeof(h));
      }

    /** Construct output directory path from
     *    base_dir and, if requested, run_number
     *    @param  o File writer option struct
     *    @return   Path of form 'base_dir/run<06d>' or 'base_dir'
     */
    static bfs::path outputDirPath(const Options& o)
      {
        bfs::path p(o.base_dir);
        if (!o.omit_run_dir) {
          std::stringstream r;
          r << std::dec << std::setfill('0') << std::setw(6) << o.run_number;
          p /= bfs::path("run" + r.str());
        }
        return p; 
      }

    /** Construct output file name
     *    @param o       File writer option struct
     *    @param link_id Link id, in this case equivalent to T-RORC DMA channel ID
     *    @return        File path of form 'run<06d>_trorc<02d>_link<02d>.bin'
     */
    static bfs::path outputFilename(const Options& o, uint32_t link_id)
      {
        std::stringstream r, t, c;
        r << std::dec << std::setfill('0') << std::setw(6) << o.run_number;
        t << std::dec << std::setfill('0') << std::setw(2) << o.device_id;
        c << std::dec << std::setfill('0') << std::setw(2) << link_id;
        return bfs::path("run" + r.str() + "_trorc" + t.str() + "_link" + c.str() + ".bin");
      }

    /** Create output directories, if it doesn't exist yet
     */
    static bool createOutputDirectory(const Options& o)
      {
        bfs::path p(outputDirPath(o));
        return bfs::exists(p) ? false : bfs::create_directories(p);
      }

    /** Filessytem checks to run at startup
     *    - Check base output directory or closest existing parent (existing? writeable?)
     *    - Check output file path (already existing? overwrite?)
     *    @params o FileWriter options struct
     *    @return True, if checks succeed, false otherwise
     */
    static bool startupCheck(const Options& o)
      {
        try {
          // Check base output directory
          bfs::path pord(o.base_dir);

          // Find base directory or closest existing parent of it
          do {
            if (bfs::exists(pord)) {
              bfs::canonical(pord);
              bfs::is_directory(pord);
            }
            else
              pord = pord.parent_path();
          }
          while (!bfs::exists(pord)); 
          // pord -> should be writeable base directory of writeable closest parent here

          // Base output dir or closest existing parent writeable?
          bfs::path test(pord);
          test  /= bfs::unique_path();
          std::ofstream test_file(test.native(), std::ios::out);
          test_file << "test\n";
          bool test_write_ok = test_file.good();
          test_file.close();
          if (!test_write_ok)
            throw(bfs::filesystem_error("Writing to directory " + pord.native(),
              boost::system::errc::make_error_code(boost::system::errc::permission_denied)));
          else
            bfs::remove(test);

          // Test if output files exist for active channels
          //   and throw if file exists and overwrite is not specified
          for (uint32_t c = 0; c < o.active_channel_mask.size(); c++) {
            if (o.active_channel_mask[c]) {
              bfs::path p = (outputDirPath(o) /= outputFilename(o, c));

              if (bfs::exists(p)) {
                if (o.overwrite)
                  Log::warn("File", p.native(), "will be overwritten");
                else
                  throw(bfs::filesystem_error(p.native(),
                    boost::system::errc::make_error_code(boost::system::errc::file_exists)));
              }
            }
          }
        }
        catch (bfs::filesystem_error& e) {
          Log::error(e.what());
          return false;
        }
        catch (...) {
          Log::error("Unknown error");
          return false;
        }

        return true;
      }
};  // FileWriter



//------------------------------------------------------------------------------
class ChannelReadout
{
  public:
    static std::mutex active_mtx_;
    static std::atomic<channel_mask_t> readout_active_mask_;

    /** Test if any channel is still active
     *    (e.g. has not yet acquired the requested number
     *    of events)
     */
    static bool anyChannelActive()
      {
        std::lock_guard<std::mutex> g(active_mtx_);
        return ChannelReadout::readout_active_mask_.load().any();
      }

    /** Request stop and trigger end of event loop
     */
    static void requestStop()
      {
        ChannelReadout::stop_requested_ = true;
      }

  private:
    static std::atomic<bool> stop_requested_;
    static const uint64_t    dmaBufferSize_ = (uint64_t)1 << 31;

    /** Flip channel bit in active readout mask
     */
    void deactivate()
      {
        std::lock_guard<std::mutex> g(active_mtx_);
        readout_active_mask_.store(readout_active_mask_.load().reset(channel_));
      }

    /** Initialize global readout thread synchronization
        @param acm Mask of active channels
     */
    void initialize(channel_mask_t acm)
      {
        std::lock_guard<std::mutex> g(active_mtx_);
        readout_active_mask_.store(acm);
        stop_requested_ = false;
      }

    std::atomic<uint64_t>    events_recorded_;
    uint64_t                 event_limit_;
    bool                     produce_sw_header_;

    std::unique_ptr<librorc::event_stream> es_;
    std::unique_ptr<trorc::GbtLink>        gbt_;
    uint32_t channel_;
    std::unique_ptr<FileWriter>            fw_;

  public:
    struct Options {
      channel_mask_t active_channel_mask;
      uint32_t event_limit;
      uint32_t readout_frame_count;
      uint32_t control_pattern_cycles;
      bool     sw_header;
    };

    /** Construct channel readout
     *  @param es  Unique pointer to channels RORC event stream, ChannelReadout takes ownership
     *  @param gbt Unique pointer to channels GBT link, ChannelReadout takes ownership
     *  @param fw  Unique pointer to channels file writer, ChannelReadout takes ownership
     @  @param o   Channel readout options struct
     */
    ChannelReadout(
#if not defined SIMULATION_NORORC
        std::unique_ptr<librorc::event_stream> es,
        std::unique_ptr<trorc::GbtLink> gbt,
#else
        uint32_t channel,
#endif
        std::unique_ptr<FileWriter> fw,
        const Options& o) :
        events_recorded_(0),
        event_limit_(o.event_limit),
        produce_sw_header_(o.sw_header),
#if not defined SIMULATION_NORORC
        es_(std::move(es)), gbt_(std::move(gbt)),
#else
        channel_(channel),
#endif
        fw_(std::move(fw))
      {
#if not defined SIMULATION_NORORC
        // Mirror info about our channel
        channel_ = es_->m_channel_status->channel;
#endif
        Log::sys("Created channel", channel_, "readout thread");

        // Initialize global synchronization variables
        //   (called multiple time by all readout thread at construction, but all
        //   threads provide the same data)
        initialize(o.active_channel_mask);

#if not defined SIMULATION_NORORC
        // GBT setup
        gbt_->stopReadout();
        gbt_->disableControlPattern();

        // Set default idle and control patterns, masks (, and cycles)
        //gbt_->setTxIdlePattern(0x10000, 0x00000fff, 0xffffffff);
        //gbt_->setTxControlPattern(0x10000, 0x00000f00, 0x000fffff);
        gbt_->setTxIdlePattern(0x10000, 0x0fffffff, 0xffffffff);
        gbt_->setTxControlPattern(0x10000, 0x0fffff00, 0x000fffff);
        gbt_->setSoftwareTriggerChannelMask(o.active_channel_mask.to_ulong());
        gbt_->setTxPatternChannelMask(o.active_channel_mask.to_ulong());
        gbt_->setTxControlPatternCycles(o.control_pattern_cycles);

        // Set frame counts (equal in our case)
        gbt_->setRxReadoutTargetFrameCount(o.readout_frame_count);  // -> Gate opening after trigger
        gbt_->setRxEventSizeFrameCount(o.readout_frame_count);      // -> DMA block size

        es_->m_channel->clearEventCount();
        es_->m_channel->clearStallCount();
        es_->m_channel->readAndClearPtrStallFlags();

        // Allow external triggers fom LVDS or trigger generator
        gbt_->setAllowExternalTrigger(1);

        int ec = es_->initializeDma(2*channel_, dmaBufferSize_);
        if (ec != 0)
          throw trorc::Exception("Failed to initialize DMA" + std::string(librorc::errMsg(ec)));
#endif
      }


    /** Destructor
     */
    ~ChannelReadout()
      {
        Log::sys("Finalizing channel", channel_, "readout");

#if not defined SIMULATION_NORORC
        gbt_->setAllowExternalTrigger(0);
        gbt_->stopReadout();
        es_->m_channel->clearEventCount();
        es_->m_channel->clearStallCount();
        es_->m_channel->readAndClearPtrStallFlags();
#endif
      }


    /** Request SYNC event from SAMPAs, and record it
     */
    void requestSyncEvent()
      {
        std::stringstream tm, rm;
        tm << std::hex << gbt_->txPatternChannelMask();
        rm << std::hex << gbt_->softwareTriggerChannelMask();
#if not defined SIMULATION_NORORC
        gbt_->driveControlPattern(true);
#endif
        Log::info("Initiated SYNC pattern and recording of event [txPatMask 0x"
             + tm.str() + ", roMask 0x" + rm.str() + "]");
      }


    /** Request recording of event
     */
    void requestEvent()
      {
#if not defined SIMULATION_NORORC
        gbt_->triggerReadout();
#endif
        Log::info("Initiated software trigger");
      }


    /** Thread worker with event loop
     */
    void run()
      {
        Log::info("Readout thread started (" +
             (event_limit_ ? ("event limit " + std::to_string(event_limit_)) :
                              "no event limit") + ")");

#if not defined SIMULATION_NORORC
        librorc::EventDescriptor *report;
        const uint32_t *event;
        uint64_t reference;
#endif

        // Event polling loop
        while (!stop_requested_) {
#if not defined SIMULATION_NORORC
          if (es_->getNextEvent(&report, &event, &reference)) {
            es_->updateChannelStatus(report);

            ssize_t event_size = ssize_t((report->calc_event_size & 0x3fffffff) << 2) + 4;

            if (produce_sw_header_)
              fw_->prependSwHeader(channel_, event_size >> 2, events_recorded_);

            fw_->write(reinterpret_cast<const char*>(event), event_size);
            es_->releaseEvent(reference);

            events_recorded_++;
            // log("   ** ev " + std::to_string(events_recorded_ - 1) + " recorded"); // TODO: Implement some stats printing?
          }
#else 
          Log::info("Recording event " + std::to_string(events_recorded_));
          std::this_thread::sleep_for(std::chrono::milliseconds(500));
          // Dummy writes for testing
          fw_->write(reinterpret_cast<char*>(&channel_), sizeof(uint32_t));
          fw_->write(reinterpret_cast<char*>(&events_recorded_), sizeof(uint64_t));
          fw_->write("deadbeaf", 8);
          events_recorded_++;
#endif
          if ((event_limit_ != 0) && (events_recorded_ >= event_limit_)) {
            Log::info("Event limit reached");
            break;
          }
        }

        fw_->flush();
        deactivate();
        Log::info("Readout done: " + std::to_string(events_recorded_) + " events recorded");
      }
}; // ChannelReadout

std::mutex ChannelReadout::active_mtx_;
std::atomic<bool> ChannelReadout::stop_requested_(false);
std::atomic<channel_mask_t> ChannelReadout::readout_active_mask_(0x0);



//------------------------------------------------------------------------------
int main(int argc, char** argv)
{
#if defined SIMULATION_NORORC
  Log::warn("*** RUNNING SIMULATION MODE WITHOUT T-RORC ACCESS ***");
#endif

  enum exit_codes_t : int {
    ec_success = 0,
    ec_err_cmdline = 1, 
    ec_err_filesystem = 2,
    ec_err_device = 3,
    ec_err_channel_readout = 4
  };

  bpo::variables_map vm;
  bpo::options_description opt_general(
      "T-RORC readout for multiple FECs\n"
      "  Each FEC provides two data streams originating at GBTx0 and GBTx1. Readout\n"
      "  is active until the event limit is reached (if set), or the runtime limit is\n"
      "  reached (if set), or a program abort is requested by the operator.\n"
      "Usage:\n  " + std::string(argv[0]) + " <cmds/options>\n"
      "Options"
    );
  bpo::options_description opt_hidden("");
  bpo::options_description opt_all;
  bpo::positional_options_description opt_pos;

  FileWriter::Options fw_options;
  ChannelReadout::Options cr_options;

  try
  {
    opt_general.add_options()
      // FEC mask (with range check 0x1..0x3f)
      ("mask,m", bpo::value<cl_uint_t>()->required()
         ->notifier([](cl_uint_t v) {
           if (v() < 0x1 || v() > ((uint32_t(1) << (fec_mask_t().size())) - 1))
             { throw bpo::validation_error(bpo::validation_error::invalid_option_value, "mask");  } } ),
         "REQUIRED: Mask of FECs to read out. Valid values are: [0x1 .. 0x3f]")

      // No sync event at start-of-readout
      ("no-sync", bpo::bool_switch(),
                  "Do no request SYNC pattern and record no SYNC event at the start of readout")

      // Number of triggered events
      ("events", bpo::value<uint32_t>(&(cr_options.event_limit))->default_value(50000),
                 "Number of triggered events to record (0 -> unlimited)")

      // Runtime
      ("runtime", bpo::value<uint32_t>()->default_value(0),
          "Specify the runtime of the readout program in seconds (0 -> unlimited)")

      // Frames per event
      ("frames", bpo::value<uint32_t>(&(cr_options.readout_frame_count))->default_value(160),
                 "Number of GBT frames to read out (per trigger)")

      // File I/O
      ("output-dir", bpo::value<std::string>(&(fw_options.base_dir))
          ->default_value("/local/data/tpc-beam-test-2017"),
          "Specify the base output directory for recorded data")

      // Overwrite
      ("overwrite", bpo::bool_switch(&(fw_options.overwrite))->default_value(false),
          "Overwrite potentially existing files output directory")

      // Set run number
      ("run-nr", bpo::value<uint32_t>(&(fw_options.run_number))->default_value(0x0),
          "Specify current run number ")

      // Run directory creation
      ("no-run-dir", bpo::bool_switch(&(fw_options.omit_run_dir)),
          "Do not create <output-dir>/run<run-nr> directory to store files in")

      // Verbosity / log level
      ("verbose,v", bpo::value<int32_t>()->default_value(Log::lsys)
         ->notifier([](int32_t v) { if ((v < Log::lsys) || (v > Log::lfatal)) {
             throw bpo::validation_error(bpo::validation_error::invalid_option_value, "verbose");  } } ),
          std::string("Control log level. Valid values are " +
                      std::to_string(Log::lsys) + " [" + Log::levelName(Log::lsys) + "] to [" +
                      std::to_string(Log::lfatal) + " [" + Log::levelName(Log::lfatal) + "]").c_str())

      ("version", "Print version information and exit")
      ("help,h", "Print this help message")
      ;

    opt_hidden.add_options()
      ("sw-header", bpo::bool_switch(&(cr_options.sw_header)),
          "Create a software header, which is prepended to the data received by the RORC")

      ("device", bpo::value<cl_uint_t>()->default_value(0),
          "Specify the RORC device ID")

      ("cp-cyc", bpo::value<cl_uint_t>()->default_value(0x10)
         ->notifier([](cl_uint_t v) { if(v() < 0x1 || v() > 0x80) {
             throw bpo::validation_error(bpo::validation_error::invalid_option_value, "cp-cyc");  } } ),
                 "Number of cycles the control pattern is driven")
      ;

    opt_all.add(opt_general).add(opt_hidden);

    bpo::store(bpo::command_line_parser(argc, argv).options(opt_all)
                     .positional(opt_pos).run(), vm);

    if (vm.count("help")) {
      std::cout << opt_general << std::endl;
      exit(ec_success);
    }

    if (vm.count("version")) {
      std::cout << GitInfo();
      exit(ec_success);
    }

    bpo::notify(vm);
  }
  catch(bpo::error& e)
  {
    Log::error(std::string(e.what()) + ".", "Exiting");
    std::cout << opt_general << std::endl;
    exit(ec_err_cmdline);
  }
  catch(std::exception& e)
  {
    Log::error(std::string(e.what()) + ".", "Exiting");
    exit(ec_err_cmdline);
  }

  //
  // Update log level
  Log::setLevel(static_cast<Log::level_t>(vm["verbose"].as<int32_t>()));

  //
  // Calculate active channel mask and lowest active channel
  fec_mask_t active_fec_mask(vm["mask"].as<cl_uint_t>()());
  channel_mask_t active_channel_mask(0);
  int lowest_channel(-1);

  for (uint32_t c = 0; c < 2*active_fec_mask.size(); c++) {
    if (active_fec_mask[c/2] && (lowest_channel == -1))
      lowest_channel = c;
    if (active_fec_mask[c/2])
      active_channel_mask[c] = true;
  }

  Log::sys("Using FEC mask:", active_fec_mask.to_string(),
           ", Channel mask:", active_channel_mask.to_string());

  //
  // Filesystem checking
  fw_options.device_id = vm["device"].as<cl_uint_t>()();
  fw_options.active_channel_mask = active_channel_mask;

  if (!FileWriter::startupCheck(fw_options)) {
    Log::error("Exiting");
    exit(ec_err_filesystem);
  }
  
  //
  // Setup device
  std::unique_ptr<trorc::Device> trorc;
  std::unique_ptr<trorc::Bar> bar;

  try {
    trorc.reset(new trorc::Device(vm["device"].as<cl_uint_t>()()));
    bar.reset(new trorc::Bar(*trorc, 1));
  }
  catch (int& e) {
    Log::fatal("Failed to initialize T-RORC:", std::to_string(e), ": ", librorc::errMsg(e));
    exit(ec_err_device);
  }

  //
  // Setup channel readout
  FileWriter::createOutputDirectory(fw_options);
  std::array<std::unique_ptr<ChannelReadout>, active_channel_mask.size()> channels;
  cr_options.control_pattern_cycles = vm["cp-cyc"].as<cl_uint_t>()();
  cr_options.active_channel_mask = active_channel_mask.to_ulong();

  try {
    for (uint32_t c = 0; c < active_channel_mask.size(); c++) {
      if (active_channel_mask[c]) {
        channels[c].reset(new ChannelReadout(
#if not defined SIMULATION_NORORC
            make_unique<librorc::event_stream>(trorc->get(), bar->get(), c, librorc::kEventStreamToHost),
            make_unique<trorc::GbtLink>(*(bar.get()), c),
#else
            c,
#endif
            make_unique<FileWriter>(fw_options, c),
            cr_options
          ));
      }
    }
  }
  catch (int& e) {
    Log::fatal("Failed setup T-RORC channel readout:", std::to_string(e), ", ", librorc::errMsg(e));
    exit(ec_err_channel_readout);
  }
  catch (trorc::Exception& e) {
    Log::fatal(e.what());
    exit(ec_err_channel_readout);
  }
  catch (std::runtime_error& e) {
    Log::fatal(e.what());
    exit(ec_err_channel_readout);
  }

  //
  // Signal handling
  signal(SIGINT, [](int signum) {
      Log::info("Termination requested, deactivating readout threads");
      ChannelReadout::requestStop();
    });

  //
  // Create thread group with one readout thread per link
  boost::thread_group readout_threads;

  for (uint32_t c = 0; c < channel_mask_t().size(); c++) {
    if (active_channel_mask[c]) {
      readout_threads.create_thread(boost::bind(&ChannelReadout::run, channels[c].get()));
    }
  }

  //
  // Request SYNC patter from SAMPAs and record associated event
  if (!vm["no-sync"].as<bool>()) {
    channels[lowest_channel]->requestSyncEvent();
  }

  //
  // Implement program runtime
  uint32_t time_limit(vm["runtime"].as<uint32_t>());
  std::chrono::time_point<std::chrono::system_clock> t_start = std::chrono::system_clock::now();
  while (ChannelReadout::anyChannelActive()) {
    std::chrono::time_point<std::chrono::system_clock> t_now = std::chrono::system_clock::now();
    if ((time_limit != 0) && 
        (std::chrono::duration_cast<std::chrono::seconds>(t_now - t_start).count() > time_limit)) {
      Log::info("Time limit reached, deactivating readout threads");
      ChannelReadout::requestStop();
      break;
    }
  }

  //
  // Wait for threads to finalize
  Log::sys("Waiting for threads to join");
  readout_threads.join_all();

  return ec_success;
}
