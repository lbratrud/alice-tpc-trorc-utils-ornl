#include <sstream>
#include <thread>
#include <bitset>

// Normal debug messaging
#ifdef HDLC_DEBUG
  #define hdlcdebug(s)  std::cout << __func__ << " : " << s << std::endl;
#else
  #define hdlcdebug(...)
#endif

// hdlc_debug1 has a lot of details
#ifdef HDLC_DEBUG1
  #define hdlcdebug1(s)  std::cout << __func__ << " : " << s << std::endl;
#else
  #define hdlcdebug1(...)
#endif

#include "gbt_sca_comm.hpp"

namespace gbt {

//------------------------------------------------------------------------------
const std::string
ScaComm::hdlcPayloadErrors_[] =
{
  "Invalid channel request",
  "Invalid command request",
  "Invalid transaction number request",
  "Invalid request length",
  "Channel disabled",
  "Channel busy",
  "Command in treatment",
  "Unknown"
};

std::string
ScaComm::getPayloadErrorMessage(uint8_t error_code) const
{
  std::stringstream os;
  for(uint8_t i = 0; i < 8; i++)
    if ((1 << i) & error_code)
      os << " +" << hdlcPayloadErrors_[i];
  return os.str();
}


#if defined SUPPORT_HDLC_SW
//------------------------------------------------------------------------------
void
GscTrorcHdlcSw::sendRequest(HdlcEcFramePayload& r)
{
  adjustRequestTrid(r);
  hdlc_sw_ec_256_t fb;
  hdlc_.iFrame(fb, r, txSwseqCnt(), rxSwseqCnt());

  txShregLoad(fb);
  rxAck();    // Allow Rx of a new frame
  txStart();  // Start Tx
  txSwseqCntInc();

  hdlcdebug("  HDLC | Sent frame :       " << r)
}


uint32_t
GscTrorcHdlcSw::getFrame(HdlcEcFrame& r) const
{
  hdlc_sw_ec_256_t fb(0);
  rxShregFetch(fb);
  std::vector<size_t> flag_pos(hdlc_.getStartFlagPositions(fb));

  // hdlcdebug("Found " << flag_pos.size() << " frames in Rx buffer");

  // Only process the first frame found in the buffer, ignore others
  uint32_t bif(0);
  if (flag_pos.size()) {
    bif = hdlc_.deFrame(r, fb);
    // std::cout << r << " (" << bif << ")" << std::endl;

    rxSwseqCntInc();

    hdlcdebug("  HDLC | Got " << r)
  }
  return ((bif > 16) && r.crcMatch()) ? bif : 0;
}


uint32_t
GscTrorcHdlcSw::execCommand(HdlcEcFramePayload& request, HdlcEcFramePayload& reply)
{
  uint8_t retries(0);
  bool    do_retry(true);
  HdlcEcFrame f;


  while (do_retry) {
    try {
      // Transmit request frame
      hdlcdebug1("  HDLC | Waiting for TX ready")
      while (!txReady()) {};
      hdlcdebug1("  HDLC | TX ready, sending request")
      sendRequest(request);
  

      // Wait for frame receive, detect Rx timeout
      std::chrono::high_resolution_clock::time_point ts = std::chrono::high_resolution_clock::now();
      hdlcdebug1("  HDLC | Waiting for RX done")
      while (!rxReady()) {
        if ((std::chrono::high_resolution_clock::now() - ts) > transceive_timeout_) {
          throw HdlcException("Rx timeout");
        }
        std::this_thread::sleep_for(std::chrono::microseconds(50));
      }
      hdlcdebug1("  HDLC | RX done, frame received, decoding")

      // Decode frame, only accept I-Frames
      if (getFrame(f) && f.isIFrame()) {
        reply = f.payload;
        do_retry = false;
      }
    }
    catch (HdlcException& e) {
      // Handle Rx exceptions
      retries++;
      if (print_hdlc_dbg_ || (retries >= transceive_max_retries_))
        std::cerr << e.what() << " -> retry " << static_cast<uint32_t>(retries) << "/"
                  << static_cast<uint32_t>(transceive_max_retries_) << std::endl;

      if (retries >= transceive_max_retries_)
        throw;
    }
  }

  return retries;
}


std::ostream&
GscTrorcHdlcSw::printStatus(std::ostream& os) const
{
  uint32_t tx_ctrl = bar_.read(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL);
  uint32_t rx_ctrl = bar_.read(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL);

  os << "SCA core " << static_cast<uint32_t>(sca_idx_) << " status (gbt_hdlc_sw) :"
     << std::hex << std::setfill('0') << std::right
     << "\n  txBaseAddr 0x" << std::setw(8) << tx_base_addr_
     << "  | rxBaseAddr 0x" << std::setw(8) << rx_base_addr_
     << "\n  txCtrl     0x" << std::setw(8) << tx_ctrl
     << "  | rxCtrl     0x" << std::setw(8) << rx_ctrl
     << std::dec << std::left << std::setfill(' ')
     << "\n    start      "   << std::setw(8) << (bar_.getBit(tx_ctrl, B_TX_START) ? "1" : "0") << "  "
     <<   "|   ack        "   << std::setw(8) << (bar_.getBit(rx_ctrl, B_RX_ACK)   ? "1" : "0")
     << "\n    done       "   << std::setw(8) << (bar_.getBit(tx_ctrl, B_TX_DONE)  ? "1" : "0") << "  "
     <<   "|   done       "   << std::setw(8) << (bar_.getBit(rx_ctrl, B_RX_DONE)  ? "1" : "0")
     << "\n    clr        "   << std::setw(8) << (bar_.getBit(tx_ctrl, B_TX_CLR)   ? "1" : "0") << "  "
     <<   "|   clr        "   << std::setw(8) << (bar_.getBit(rx_ctrl, B_RX_CLR)   ? "1" : "0")
     << "\n    bitswap    "   << std::setw(8) << (bar_.getBit(tx_ctrl, B_TX_BITSWAP_SEL)  ? "1" : "0") << "  "
     <<   "|   bitswap    "   << std::setw(8) << (bar_.getBit(rx_ctrl, B_RX_BITSWAP_SEL)  ? "1" : "0")
     << "\n    seqCnt     "   << std::setw(8) << static_cast<uint32_t>(bar_.getField(tx_ctrl, F_TX_SEQ_CNT_LSB, F_TX_SEQ_CNT_WDT)) << "  "
     <<   "|   seqCnt     "   << std::setw(8) << static_cast<uint32_t>(bar_.getField(rx_ctrl, F_RX_SEQ_CNT_LSB, F_RX_SEQ_CNT_WDT))
     << "\n    swInc      "   << std::setw(8) << (bar_.getBit(tx_ctrl, B_TX_SWSEQ_CNT_INC)   ? "1" : "0") << "  "
     <<   "|   swInc      "   << std::setw(8) << (bar_.getBit(rx_ctrl, B_RX_SWSEQ_CNT_INC)   ? "1" : "0")
     << "\n    swSeqCnt   "   << std::setw(8) << static_cast<uint32_t>(txSwseqCnt()) << "  "
     <<   "|   swSeqCnt   "   << std::setw(8) << static_cast<uint32_t>(rxSwseqCnt())
     << "\n";
  for (uint32_t i = 0; i < 8; i++)
    os << std::hex << std::setfill('0') << std::right
       <<   "  txData" << i << "    0x" << std::setw(8) << bar_.read(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_DATA_0 + i)
       << "  | rxData" << i << "    0x" << std::setw(8) << bar_.read(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_DATA_0 + i)
       << "\n" << std::dec;
  return os;
}



/**
 * SOME DEBUGGING BELOW
 */
void
GscTrorcHdlcSw::dbg0()
{
  // DATA=0x7f7f7f7f7f7f7f7f7f7f7f7f7e70b8000000000000addeabab210402ab08007e
  uint32_t d[] = {
      0xab08007e,
      0xab210402,
      0x00addeab,
      0x00000000,
      0x7e70b800,
      0x7f7f7f7f,
      0x7f7f7f7f,
      0x7f7f7f7f
    };
  for (uint8_t i = 0; i < 8; i++)
    bar_.write(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_DATA_0 + i, d[i]);

  bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_ACK, 0x1);
  bar_.writeBit(rx_base_addr_ + TRORC_REG_GBTRX_SCA_SW_CTRL, B_RX_ACK, 0x0);
  bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_START, 0x1);
  bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_SW_CTRL, B_TX_START, 0x0);
}

void
GscTrorcHdlcSw::dbg1()
{
  HdlcEcFrame r;
  getFrame(r);
}
#endif  // SUPPORT_HDLC_SW


#if defined SUPPORT_HDLC_LIGHT
//------------------------------------------------------------------------------
void
GscTrorcHdlcLight::sendRequest(HdlcEcFramePayload& r)
{
  uint32_t p_l;
  uint32_t p_h;

  adjustRequestTrid(r);

  // Assemble and send request
  assembleRegisters(r, p_l, p_h);
  bar_.write(tx_base_addr_ + TRORC_REG_GBTTX_SCA_PAYLOAD_LOW, p_l);
  bar_.write(tx_base_addr_ + TRORC_REG_GBTTX_SCA_PAYLOAD_HIGH, p_h);
  bar_.writeField(tx_base_addr_ + TRORC_REG_GBTTX_SCA_CTRL, F_TX_HDLC_ADDR_LSB, F_TX_HDLC_ADDR_WDT, 0x0);
  bar_.writeField(tx_base_addr_ + TRORC_REG_GBTTX_SCA_CTRL, F_TX_MODE_LSB, F_TX_MODE_WDT, svl_packet);
  bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_CTRL, B_TX_START, 0x1);
  bar_.writeBit(tx_base_addr_ + TRORC_REG_GBTTX_SCA_CTRL, B_TX_START, 0x0);

  hdlcdebug("  HDLC | Sent frame " << r)
}


bool
GscTrorcHdlcLight::getReplyCrc(uint32_t& crcRecalc, uint32_t& crcReceived) const
{
  uint32_t tmp(bar_.read(rx_base_addr_ + TRORC_REG_GBTRX_SCA_CRC));
  crcRecalc = bar_.getField(tmp, F_RX_CRC_RECALC_LSB, F_RX_CRC_RECALC_WDT);
  crcReceived = bar_.getField(tmp, F_RX_CRC_RECEIVED_LSB, F_RX_CRC_RECEIVED_WDT);
  return crcRecalc == crcReceived;
}


uint32_t
GscTrorcHdlcLight::getReply(HdlcEcFramePayload& r) const
{
  uint32_t p_l;
  uint32_t p_h;
  uint32_t ec(success);

  p_l = bar_.read(rx_base_addr_ + TRORC_REG_GBTRX_SCA_PAYLOAD_LOW);
  p_h = bar_.read(rx_base_addr_ + TRORC_REG_GBTRX_SCA_PAYLOAD_HIGH);
  disassembleRegisters(r, p_l, p_h);

  // TODO: Handle CRC check already here?
  if(r.channel && r.error)
    ec = r.error;

  hdlcdebug("  HDLC | Rcvd frame " << r)
  return ec;
}


uint32_t
GscTrorcHdlcLight::execCommand(HdlcEcFramePayload& request, HdlcEcFramePayload& reply)
{
  while (!txReady());
  sendRequest(request);
  while (!rxReady());
  uint32_t ec = getReply(reply);

  uint32_t crc1;
  uint32_t crc2;

  // TODO: implement better error checking/handling
  if ((reply.trid == request.trid) && getReplyCrc(crc1, crc2))
    return success;
  return ec;
}


void
GscTrorcHdlcLight::sendSvlCommand(svl_cmd_codes_t c) const
{}


std::ostream&
GscTrorcHdlcLight::printStatus(std::ostream& os) const
{
  uint32_t tx_ctrl = bar_.read(tx_base_addr_ + TRORC_REG_GBTTX_SCA_CTRL);
  uint32_t rx_ctrl = bar_.read(rx_base_addr_ + TRORC_REG_GBTRX_SCA_CTRL);
  uint32_t rx_crc  = bar_.read(rx_base_addr_ + TRORC_REG_GBTRX_SCA_CRC);
  uint64_t rx_payload = (static_cast<uint64_t>(bar_.read(rx_base_addr_ + TRORC_REG_GBTRX_SCA_PAYLOAD_HIGH)) << 32) |
                        bar_.read(rx_base_addr_ + TRORC_REG_GBTRX_SCA_PAYLOAD_LOW);
  uint64_t tx_payload = (static_cast<uint64_t>(bar_.read(tx_base_addr_ + TRORC_REG_GBTTX_SCA_PAYLOAD_HIGH)) << 32) |
                        bar_.read(tx_base_addr_ + TRORC_REG_GBTTX_SCA_PAYLOAD_LOW);

  os << "HDLC light core status:"
     << std::left
     << "\n  txStart         "   << std::setw(18) << (bar_.getBit(tx_ctrl, B_TX_START) ? "1" : "0") << "  "
     <<   "| rxStart         "   << std::setw(18) << (bar_.getBit(rx_ctrl, B_RX_START) ? "1" : "0") << "  "
     << "\n  txReady         "   << std::setw(18) << (bar_.getBit(tx_ctrl, B_TX_READY) ? "1" : "0") << "  "
     <<   "| rxReady         "   << std::setw(18) << (bar_.getBit(rx_ctrl, B_RX_READY) ? "1" : "0") << "  "
     << std::hex
     << "\n  txMode          0x" << std::setw(18) << bar_.getField(tx_ctrl, F_TX_MODE_LSB, F_TX_MODE_WDT)
     <<   "| rxSvlCmd        0x" << std::setw(18) << (bar_.getBit(rx_ctrl, B_RX_SVL_CMD) ? "1" : "0") << "  "
     << "\n  txSeqNr         0x" << std::setw(18) << bar_.getField(tx_ctrl, F_TX_SEQNR_LSB, F_TX_SEQNR_WDT)
     <<   "| rxSeqNr         0x" << std::setw(18) << bar_.getField(rx_ctrl, F_RX_SEQNR_LSB, F_RX_SEQNR_WDT)
     << "\n  txFsmState      0x" << std::setw(18) << bar_.getField(tx_ctrl, F_TX_FSM_LSB, F_TX_FSM_WDT)
     <<   "| rxFsmState      0x" << std::setw(18) << bar_.getField(rx_ctrl, F_RX_FSM_LSB, F_RX_FSM_WDT)
     << "\n  txAddr          0x" << std::setw(18) << bar_.getField(tx_ctrl, F_TX_HDLC_ADDR_LSB, F_TX_HDLC_ADDR_WDT)
     <<   "| rxAddr          0x" << std::setw(18) << bar_.getField(rx_ctrl, F_RX_HDLC_ADDR_LSB, F_RX_HDLC_ADDR_WDT)
     << "\n                    " << std::setw(18) << ""
     <<   "| rxCtrl          0x" << std::setw(18) << bar_.getField(rx_ctrl, F_RX_HDLC_CTRL_LSB, F_RX_HDLC_CTRL_WDT)
     << "\n                    " << std::setw(18) << ""
     << std::right << std::setfill('0')
     <<   "| rxCrc rcv/calc  0x" << std::setw(4) << bar_.getField(rx_crc, F_RX_CRC_RECEIVED_LSB, F_RX_CRC_RECEIVED_WDT)
     <<                  " / 0x" << std::setw(4) << bar_.getField(rx_crc, F_RX_CRC_RECALC_LSB, F_RX_CRC_RECALC_WDT)
     << "\n  txPayload       0x" << std::setw(16) << tx_payload << "  "
     <<   "| rxPayload       0x" << std::setw(16) << rx_payload << "  "
     << std::setfill(' ') << std::dec
     << "\n";
  return os;
}
#endif  // SUPPORT_HDLC_LIGHT

}

#undef hdlcdebug
#undef hdlcdebug1
