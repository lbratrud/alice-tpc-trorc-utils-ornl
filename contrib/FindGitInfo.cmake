add_custom_target(updateGitInfo ALL 
    ${CMAKE_SOURCE_DIR}/contrib/updateGitInfo.sh ${CMAKE_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Updating git information"
    VERBATIM
  )

include_directories(${CMAKE_CURRENT_BINARY_DIR})
